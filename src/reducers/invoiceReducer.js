import {
    CREATE_INVOICE,
    EDIT_INVOICE,
    FETCH_INVOICE,
    FETCH_INVOICES,
    CLEAR_NEW_INVOICE_STATUS,
    CLEAR_EDIT_INVOICE_SUCCESS,
    SET_SINGLE_INVOICE_VIEW,
    CLEAR_SINGLE_INVOICE_VIEW,
    SET_INVOICE_VIEW
} from '../actions/types';

const initialState = {
    status:{},
    allInvoices:{},
    singleInvoice:{},
    views:{
        editSuccess: '',
        singleView: false,
        view: ''
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case CREATE_INVOICE:
            return{...state, ...action.payload};
        case FETCH_INVOICES:
            return{...state, ...action.payload};
        case EDIT_INVOICE:
            return{...state, ...action.payload}
        case CLEAR_NEW_INVOICE_STATUS:
            return{...state, ...action.payload};
        case SET_SINGLE_INVOICE_VIEW:
            return{...state, singleInvoice: action.payload, views:{...state.views, editSuccess: '', singleView: true}};
        case SET_INVOICE_VIEW:
            return{...state, views:{...state.views, view: action.payload}}
        case CLEAR_SINGLE_INVOICE_VIEW:
            return{...state, singleInvoice: {}, views:{...state.views, singleView: false}}
        case CLEAR_EDIT_INVOICE_SUCCESS:
            return{...state, views:{...state.views, editSuccess: ''}}
        default:
            return state;
    }
}