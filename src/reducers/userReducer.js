import{ 
    EDIT_USER,
    FETCH_USER,
    CREATE_USER      
} from '../actions/types';

export default(state = {}, action) => {
   
    switch(action.type){
        case FETCH_USER:
            return {...state, [action.payload.id]: action.payload};

        case EDIT_USER:
            return {...state, [action.payload.id]: action.payload}

        case CREATE_USER:
            return {...state, [action.payload.id]: action.payload}

        default:
            return state;
    }
}