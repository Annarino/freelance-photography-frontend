import { FETCH_PHOTOS, CLEAR_PHOTOS } from '../actions/types';

export default (state={}, action) => {
    switch(action.type){
        case FETCH_PHOTOS:
            return action.payload;
        case CLEAR_PHOTOS:
            return {};
        default:
            return state;
    }
}