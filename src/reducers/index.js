import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import userReducer from './userReducer';
import authReducer from './authReducer';
import sidebarReducer from './sidebarReducer';
import albumsReducer from './albumsReducer';
import photosReducer from './photosReducer';
import nestedViewReducer from './nestedViewReducer';
import applicationReducer from './applicationReducer';
import profileReducer from './profileReducer';
import invoiceReducer from './invoiceReducer';
import projectReducer from './projectReducer';
import supplierReducer from './supplierReducer';
import bidReducer from './bidReducer';
import fileUploadReducer from './fileUploadReducer';

export default combineReducers({
    form: formReducer,
    user: userReducer,
    auth: authReducer,
    sidebar: sidebarReducer,
    albums: albumsReducer,
    nestedView: nestedViewReducer,
    photos: photosReducer,
    application: applicationReducer,
    profile: profileReducer,
    invoice: invoiceReducer,
    project: projectReducer,
    supplier: supplierReducer,
    bid: bidReducer,
    file: fileUploadReducer
});