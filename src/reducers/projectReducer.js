import{
    CREATE_PROJECT,
    EDIT_PROJECT,
    FETCH_PROJECTS,
    FETCH_PROJECT,
    DELETE_PROJECT,
    SET_PROJECT_VIEW,
    CLEAR_PROJECT_VIEW,
    CLEAR_PROJECT_STATUS,
    SET_PROJECT_STATUS,
    SET_SINGLE_PROJECT,
    ALL,
    SINGLE,
    ADD,
    CLEAR,
    SUCCESS,
    ERROR
} from '../actions/types'

const initialState = {
    singleProject:{},
    allProjects:{},
    supplier:{},
    suppliers:{},
    views:{
        status: '',
        view: ALL
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case CREATE_PROJECT:
            return{...state,  views:{...state.views, status:SUCCESS, view: ADD}}
        case EDIT_PROJECT:
            return{...state, views:{...state.views, status:SUCCESS}}
        case FETCH_PROJECTS:
            return{...state, allProjects: action.payload, views:{...state.views, status:SUCCESS}}
        case FETCH_PROJECT:
            return{...state, singleProject: action.payload, views:{...state.views, status:SUCCESS}}
        case DELETE_PROJECT:
            return{...state, views:{...state.view, status:SUCCESS}}
        case SET_PROJECT_VIEW:
            return{...state, views:{...state.views, view: action.payload}}
        case CLEAR_PROJECT_VIEW:
            return{...state, views:{...state.views, view: ALL}}
        case CLEAR_PROJECT_STATUS:
            return{...state, views:{...state.views, status: CLEAR}}
        case SET_PROJECT_STATUS:
            return{...state, views:{...state.views, status: action.payload}}
        case SET_SINGLE_PROJECT:
            return{...state, singleProject: action.payload};
        
        default:
            return state
    }
}