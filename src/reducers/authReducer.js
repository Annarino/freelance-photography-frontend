import{
    INITIALISE,
    SIGN_IN,
    SIGN_OUT
} from '../actions/types';

const INITIAL_STATE = {
    isSignedIn: null,
    userType: null
};

export default(state = INITIAL_STATE, action) => {
    let isSignedIn = localStorage.getItem('isSignedIn');
    if (typeof isSignedIn !== "boolean" && isSignedIn !== "null" && isSignedIn !== null){
        if(isSignedIn.toLowerCase() === "true"){
            isSignedIn = true;
        } else {
            isSignedIn = false;
        }
    }
    const userStatus = localStorage.getItem('userStatus');
    switch(action.type){
        case INITIALISE:
            if(isSignedIn && action.payload.status){
                return{...state, isSignedIn, userType: action.payload.userType, userStatus:action.payload.status};
            }
            return state;
        case SIGN_IN:
            localStorage.setItem('authToken', action.payload.token);
            localStorage.setItem('userType', action.payload.userType);
            localStorage.setItem('userStatus', action.payload.status)
            localStorage.setItem('isSignedIn', true)
            return {...state, isSignedIn: true, userType: action.payload.userType, userStatus: action.payload.status};
        case SIGN_OUT:
            localStorage.removeItem('authToken');
            localStorage.removeItem('userType');
            localStorage.removeItem('isSignedIn');
            localStorage.removeItem('userStatus')
            return {...state, isSignedIn: false, userType: null, userStatus: null};
        default:
            return state;
    }
}