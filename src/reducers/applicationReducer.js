import {
    FETCH_SUPPLIER_APPLICATION ,
    SUBMIT_APPLICATION,
    SUBMIT_APPLICATION_ERROR
} from '../actions/types';

export default(state = {status: 'not_submitted'}, action) => {
    switch(action.type){
        case FETCH_SUPPLIER_APPLICATION:
            return{...state, ...action.payload}
        case SUBMIT_APPLICATION:
            return{...state, status: action.payload.status}
        case SUBMIT_APPLICATION_ERROR:
            return{...state, error: true, errorStatus: action.payload}
        default:
            return(state);
    }
}