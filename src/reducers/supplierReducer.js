import{
    FETCH_SUPPLIERS,
    FETCH_SUPPLIER,
    SET_SUPPLIER_VIEW,
    CLEAR_SUPPLIER_VIEW,
    SET_SINGLE_SUPPLIER,
    CLEAR_SINGLE_SUPPLIER,
    SET_SUPPLIER_STATUS,
    CLEAR_SUPPLIER_STATUS,
    SET_SUPPLIERS,
    ALL,
    SINGLE,
    CLEAR,
    SUCCESS,
    ERROR
}from '../actions/types'

const initialState={
    supplier:{},
    suppliers:{},
    views:{
        status:CLEAR,
        view: ALL
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_SUPPLIER:
            return{...state, supplier: action.payload, views:{...state.views, status:SUCCESS}};
        case FETCH_SUPPLIERS:
            return{...state, suppliers: action.payload, views:{...state.views, status:SUCCESS}};
        case SET_SINGLE_SUPPLIER:
            return{...state, supplier:action.payload}
        case SET_SUPPLIERS:
            return{...state, suppliers:action.payload}
        case CLEAR_SINGLE_SUPPLIER:
            return{...state, supplier:{}}
        case SET_SUPPLIER_VIEW:
            return{...state, views:{...state.views, view:action.payload}}
        case CLEAR_SUPPLIER_VIEW:
            return{...state, views:{...state.views, view:ALL}}
        case SET_SUPPLIER_STATUS:
            return{...state, views:{...state.views, status:action.payload}};
        case CLEAR_SUPPLIER_STATUS:
            return{...state, views:{...state.views, status:CLEAR}}
        default:
            return state;
    }
}