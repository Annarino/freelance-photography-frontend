import{
    CREATE_BID,
    EDIT_BID,
    DELETE_BID,
    FETCH_PROJECT_BIDS,
    FETCH_SUPPLIER_BIDS,
    FETCH_BID,
    SET_SINGLE_BID,
    CLEAR_SINGLE_BID,
    SET_BID_STATUS,
    CLEAR_BID_STATUS,
    SET_BID_VIEW,
    CLEAR_BID_VIEW
    
}from '../actions/types';

import{ALL, SINGLE, ADD, EDIT, CLEAR, SUCCESS, ERROR, LOADING} from '../actions/types';

const initialState ={
    bid: {},
    bids:{},
    views:{
        status: '',
        view: ''
    }
}

export default (state = initialState, action) => {
    switch(action.type){
        case CREATE_BID:
            return{...state, bid: action.payload, views:{...state.views, status:SUCCESS}};
        case EDIT_BID:
            return{...state, bid: action.payload, views:{...state.views, status:SUCCESS}};
        case DELETE_BID:
            return{...state, views:{...state.views, status:SUCCESS}};
        case FETCH_PROJECT_BIDS:
            return{...state, bids:action.payload, views:{...state.views, status:SUCCESS}};
        case FETCH_SUPPLIER_BIDS:
            return{...state, bids:action.payload, views:{...state.views, status:SUCCESS}};
        case FETCH_BID:
            return{...state, bid:action.payload, views:{...state.views, status:SUCCESS}};
        case SET_SINGLE_BID:
            return{...state, bid:action.payload};
        case CLEAR_SINGLE_BID:
            return{...state, bid:{}};
        case SET_BID_STATUS:
            return{...state, views:{...state.views, status:action.payload}};
        case CLEAR_BID_STATUS:
            return{...state, views:{...state.views, status:CLEAR}};
        case SET_BID_VIEW:
            return{...state, views:{...state.views, view:action.payload}};
        case CLEAR_BID_VIEW:
            return{...state, views:{...state.views, view:CLEAR}};
        default:
            return state;
    }
}