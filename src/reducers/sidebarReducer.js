import {
    CREATE_SIDEBAR,
    UPDATE_SIDEBAR,
    CLEAR_SIDEBAR
} from '../actions/types';


export default (state = {}, action) => {
    switch(action.type){
        case CREATE_SIDEBAR:
            return {...state, ...action.payload, [Object.keys(action.payload)[0]]: 'active item', wasActive: Object.keys(action.payload)[0], willBeActive:  Object.keys(action.payload)[0]};
        
        case UPDATE_SIDEBAR:
            return{...state, [action.payload.wasActive]: 'item', [action.payload.willBeActive]: 'active item', wasActive: action.payload.wasActive, willBeActive: action.payload.willBeActive}

        case CLEAR_SIDEBAR:
            return {};

        default:
            return state;
    }
}
