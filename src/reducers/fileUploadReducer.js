import{
    ADD_FILE,
    ADD_FILES,
    CLEAR_FILE,
    CLEAR_FILES,
    CLEAR_FILE_STATUS,
    SUCCESS,
    ERROR,
    CLEAR
} from '../actions/types';

const initialState = {
    file:[],
    files:[],
    status: CLEAR
}

export default (state = initialState, action) => {
    switch(action.type){
        case ADD_FILE:
            return{...state, file: action.payload, status: SUCCESS};
        case ADD_FILES:
            return {...state, files: action.payload, status: SUCCESS}
        case CLEAR_FILE:
            return {...state, file: CLEAR}
        case CLEAR_FILES:
            return{...state, files: CLEAR};
        case CLEAR_FILE_STATUS:
            return{...state, status: CLEAR}
        default:
            return state;
    }
}