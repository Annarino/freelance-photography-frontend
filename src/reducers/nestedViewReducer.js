import {
    SET_NESTED_VIEW,
    CLEAR_NESTED_VIEW
} from '../actions/types'

export default (state={}, action) => {
    switch(action.type){
        case SET_NESTED_VIEW:
            return {...state, ...action.payload}

        case CLEAR_NESTED_VIEW:
            return {}

        default:
            return state;
    }
}