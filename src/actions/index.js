import server from '../apis/server';
import jsonSamples from '../apis/jsonSamples';
import history from '../components/global/history';
import {
    INITIALISE,
    SIGN_IN,
    SIGN_OUT,
    EDIT_USER,
    EDIT_PROFILE,
    FETCH_ALBUMS,
    FETCH_PHOTOS,
    FETCH_SUPPLIER_APPLICATION,
    FETCH_USER,
    SUBMIT_APPLICATION,
    SUBMIT_APPLICATION_ERROR,
    CREATE_USER,
    CREATE_SIDEBAR,
    UPDATE_SIDEBAR,
    CLEAR_SIDEBAR,
    SET_NESTED_VIEW,
    CLEAR_NESTED_VIEW,
    CLEAR_PHOTOS,
    FETCH_PROFILE,
    FETCH_INVOICE,
    FETCH_INVOICES,
    CREATE_INVOICE,
    EDIT_INVOICE,
    DELETE_INVOICE,
    CLEAR_EDIT_INVOICE_SUCCESS,
    SET_SINGLE_INVOICE_VIEW,
    SET_INVOICE_VIEW,
    CLEAR_SINGLE_INVOICE_VIEW,
    CLEAR_NEW_INVOICE_STATUS
} from './types';

//AUTH AND INITIALISER ACTIONS
export const initialise = () => async dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    try{
        const response = await server.get('/users/user');
        dispatch({
            type: INITIALISE,
            payload: response.data
        })
    }
    catch(err){
        console.log(err);
    }
};

export const signIn = (formValues) => dispatch => {
    server
        .post('/users/login', formValues)
        .then( response => {
            dispatch({ type: SIGN_IN, payload: response.data});
            return response
        })
        .then(response => {
            server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
            if(response.data.status === 'pending'){
                console.log('checking app status');
                return server.get('/applications/supplier-application')
            }
            console.log('user is accepted')
            return response;
        })
        .then(response => {
            console.log('response status: '+ JSON.stringify(response))
            if(response.data.status === 'pending' || response.data.status === 'accepted' ){
                return history.push('/dashboard')
            }
            return history.push('/welcome')
        })
        .catch(err => {
            console.log('errored: '+ err)
        })
}


export const signOut = () => {
    history.push('/login');
    server.defaults.headers.common['Authorization'] = '';
    return{
        type: SIGN_OUT
    };
    
};


//APPLICATION ACTIONS
export const fetchSupplierApplication = () => async dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    const response = await server.get('/applications/supplier-application');
    console.log(response);
    if(response){
        dispatch({type: FETCH_SUPPLIER_APPLICATION, payload: response})
    }
    else(
        dispatch({type: FETCH_SUPPLIER_APPLICATION, payload: {status: 'not_submitted'}})
    )
}

export const submitApplication = (formValues) => async dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    const application = {
        userType: formValues.userType,
        firstName: formValues.firstName,
        surname: formValues.surname,
        phoneNumber: formValues.phoneNumber,
        email: formValues.email,
        address:{
            streetNumName: formValues.streetNumName,
            houseNumName: formValues.houseNumName,
            city: formValues.city,
            province: formValues.province,
            country: formValues.country,
            postcode: formValues.postcode
        },
        references: formValues.references,
        yearsExperience: formValues.yearsExperience,
        portfolioLinks: formValues.portfolioLinks
    }

    server
        .post('/applications', application)
        .then(response => {
            return dispatch({
                type: SUBMIT_APPLICATION,
                payload: response.data
            })
        })
        .then(() => {
            return history.push('/dashboard')
        })
        .catch(err => {
            dispatch({
                type: SUBMIT_APPLICATION_ERROR,
                payload: err.response.status
            })
        })
}


//USER ACTIONS
export const editUser = (formValues, id) => async (dispatch) => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    const response = await server.put(`/users/${id}`, formValues);
    dispatch ({ type: EDIT_USER, payload: response});
};

export const fetchUser = id => async dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    const response = await server.get(`/Users/userId=${id}`);
    dispatch({ type: FETCH_USER, payload: response.data });
};

export const createUser = (type, id, formValues) => (dispatch, getState) => {
    server
        .post('/users/signup', formValues)
        .then(response => {
            if(response.status > 300){
                const err = new Error({
                    status: response.status
                })
                throw err
            }
            return server.post('/users/login', formValues);
        })
        .then(response => {
            console.log('created resp: '+ JSON.stringify(response))
            console.log('created app: '+ JSON.stringify(formValues))

            if(response.status > 300){
                const err = new Error({
                    status: response.status
                })
                throw err
            }
            dispatch({
                type: SIGN_IN,
                payload: response.data
            });
            return ;
        })
        .then(() => history.push('/welcome'))
        .catch(err => console.log(err))
};

//PROFILE ACTIONS
export const fetchProfile = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server
        .get('/profiles')
        .then(response => {
            return dispatch({
                type: FETCH_PROFILE,
                payload: response.data
            })
        })
        .catch(err => console.log(JSON.stringify(err)))
}

export const editProfile = (formValues) => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server
        .put('/profiles', formValues)
        .then(response => {
            return dispatch({
                type: EDIT_PROFILE,
                payload: response.data
            })
        })
}

//Invoice Actions
export const createInvoice = formValues => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: CREATE_INVOICE,
        payload: {addInvoice: {status:'loading'}}
    })
    server
        .post('/invoices', formValues)
        .then(response => {
            return dispatch({
                type: CREATE_INVOICE,
                payload: {addInvoice: {status: response.status, ...response.data}}
            })
        })
        .catch(err =>{ 
            console.log(JSON.stringify(err))
            dispatch({
            type: CREATE_INVOICE,
            payload: err.response.status
            })
        });
}

export const fetchInvoices = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server
        .get('/invoices')
        .then(response => {
            dispatch({
                type: FETCH_INVOICES,
                payload: {allInvoices:{...response.data}}
            })
        })
        .catch(err => console.log(JSON.stringify(err)))
}

export const editInvoice = (formValues, formId) => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server
        .put(`/invoices/${formId}`, formValues)
        .then(response => {
            dispatch({
                type: EDIT_INVOICE,
                payload: {views:{editSuccess: true}, singleInvoice:{...response.data}}
            })
            return server.get('/invoices')
        })
        .then(response => {
            dispatch({
                type: FETCH_INVOICES,
                payload: {allInvoices:{...response.data}}
            })
        })
        .catch(err => console.log(JSON.stringify(err)))
}

export const deleteInvoice = invoiceId => dispatch =>{
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server  
        .delete(`/invoices/${invoiceId}`)
        .then(response => {
            return server.get('/invoices')
        })
        .then(response => {
            dispatch({
                type:FETCH_INVOICES,
                payload:{allInvoices: {...response.data}}
            })
            dispatch({type: CLEAR_SINGLE_INVOICE_VIEW})
        })
        .catch(err => console.log(JSON.stringify(err)))
}

export const clearNewInvoiceStatus = () => {
    return{
        type: CLEAR_NEW_INVOICE_STATUS,
        payload: {addInvoice: {status: ' '}}
    }
}

export const setSingleInvoiceView = (invoice) => {
    return {
        type: SET_SINGLE_INVOICE_VIEW,
        payload: invoice
    }
}

export const setInvoiceView = view =>{
    return{
        type: SET_INVOICE_VIEW,
        payload: view
    }
}

export const clearSingleInvoiceView = () => {
    return {
        type: CLEAR_SINGLE_INVOICE_VIEW
    }
}

export const clearEditSuccess = () => {
    return{
        type: CLEAR_EDIT_INVOICE_SUCCESS
    }
}


///////////////////////////////////////////////////////////
export const createSidebar = (items) => {
    console.log("items",items);
    return({
        type: CREATE_SIDEBAR,
        payload: items
    });
}

export const updateSidebar = (wasActive, willBeActive) => {
    return ({
        type: UPDATE_SIDEBAR,
        payload: {wasActive, willBeActive} 
    });
}

export const clearSidebar = () => {
    return {type: CLEAR_SIDEBAR}
}

export const fetchAlbums = (userId) => async dispatch => {
    const response = await jsonSamples.get(`/albums?userId=${userId}`);
    dispatch ({
        type: FETCH_ALBUMS,
        payload: response.data
        });
}

export const setNestedView = view => {
    return{
        type: SET_NESTED_VIEW,
        payload: view
    }
}

export const clearNestedView = () => {
    return{
        type: CLEAR_NESTED_VIEW
    }
}

export const fetchPhotos = (albumId) => async dispatch => {
    const response = await jsonSamples.get(`/photos?albumId=${albumId}`);
    dispatch ({
        type: FETCH_PHOTOS,
        payload: response.data
    })
}

export const clearPhotos = () =>{
    return{
        type: CLEAR_PHOTOS
    }
}