import server from '../apis/server';
import history from '../components/global/history';

import{
    FETCH_SUPPLIERS,
    FETCH_SUPPLIER,
    SET_SUPPLIER_VIEW,
    SET_SUPPLIERS,
    CLEAR_SUPPLIER_VIEW,
    SET_SINGLE_SUPPLIER,
    CLEAR_SINGLE_SUPPLIER,
    SET_SUPPLIER_STATUS,
    CLEAR_SUPPLIER_STATUS,
    ALL,
    SINGLE,
    CLEAR,
    SUCCESS,
    ERROR
}from './types'

export const setSuppliers = suppliers =>{
    return({
        type: SET_SUPPLIERS,
        payload: suppliers
    })
}


export const fetchSuppliers = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_SUPPLIER_STATUS,
        payload: 'loading'
    })
    server
        .get('/profiles/type/supplier')
        .then(response => {
            console.log('suppliers: '+ JSON.stringify(response.data))
            dispatch({
                type: FETCH_SUPPLIERS,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_SUPPLIER_STATUS,
                payload: ERROR
            })
        })
}

export const fetchSupplier = supplierId => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_SUPPLIER_STATUS,
        payload: 'loading'
    })
    server
        .get(`suppliers/${supplierId}`)
        .then(response => {
            dispatch({
                type: FETCH_SUPPLIER,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_SUPPLIER_STATUS,
                payload: ERROR
            })
        })
}

export const setSupplierView = view => {
    return({
        type: SET_SUPPLIER_VIEW,
        payload: view
    })
}

export const clearSupplierView = () => {
    return({
        type: CLEAR_SUPPLIER_VIEW
    })
}

export const setSingleSupplier = supplier => {
    return({
        type: SET_SINGLE_SUPPLIER,
        payload: supplier
    })
}

export const clearSingleSupplier = () => {
    return({
        type: CLEAR_SINGLE_SUPPLIER
    })
}

export const setSupplierStatus = status => {
    return({
        type: SET_SUPPLIER_STATUS,
        payload: status
    })
}

export const clearSupplierStatus = () => {
    return({
        type: CLEAR_SUPPLIER_STATUS
    })
}