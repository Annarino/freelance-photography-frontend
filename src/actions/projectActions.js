import server from '../apis/server';
import history from '../components/global/history';
import{
    CREATE_PROJECT,
    EDIT_PROJECT,
    FETCH_PROJECTS,
    FETCH_PROJECT,
    DELETE_PROJECT,
    SET_PROJECT_VIEW,
    CLEAR_PROJECT_VIEW,
    CLEAR_PROJECT_STATUS,
    SET_PROJECT_STATUS,
    SET_SINGLE_PROJECT

} from './types';
import{ALL, SINGLE, CLEAR, SUCCESS, ERROR, LOADING} from './types';


export const setProjectStatus = status => {
    return{
        type: SET_PROJECT_STATUS,
        payload: status
    }
}

export const clearProjectStatus = () => {
    return{
        type: CLEAR_PROJECT_STATUS
    }
}

export const setProjectView = view => {
    return{
        type: SET_PROJECT_VIEW,
        payload: view
    }
}

export const setSingleProject = project => {
    return{
        type: SET_SINGLE_PROJECT,
        payload: project
    }
}

export const clearProjectView = view => {
    return{
        type: CLEAR_PROJECT_VIEW
    }
}

export const createProject = formValues => dispatch =>{
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .post('/projects', formValues)
        .then(response => {
            dispatch({
                type: CREATE_PROJECT,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}

export const editProject = formValues => dispatch =>{
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .put(`/projects/${formValues._id}`, formValues)
        .then(response => {
            dispatch({
                type: EDIT_PROJECT,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}

export const fetchProject = (projectId) => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .get(`/projects/project/${projectId}`)
        .then(response => {
            console.log(JSON.stringify(response.data))
            dispatch({
                type: FETCH_PROJECT,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}

export const fetchUsersProjects = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .get('/projects/users-projects')
        .then(response => {
            dispatch({
                type: FETCH_PROJECTS,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}

export const fetchProjects = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .get('/projects')
        .then(response => {
            dispatch({
                type: FETCH_PROJECTS,
                payload: response.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}

export const deleteProject = (projectId) => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type: SET_PROJECT_STATUS,
        payload: LOADING
    })
    server
        .delete(`/projects/${projectId}`)
        .then(response => {
            dispatch({
                type: DELETE_PROJECT
            })
        })
        .catch(err => {
            dispatch({
                type: SET_PROJECT_STATUS,
                payload: ERROR
            })
        })
}


