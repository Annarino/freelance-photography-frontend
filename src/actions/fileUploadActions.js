import server from '../apis/server'
import{
    ADD_FILE,
    ADD_FILES,
    CLEAR_FILE,
    CLEAR_FILES,
    CLEAR_FILE_STATUS,
    SUCCESS,
    ERROR,
    CLEAR
} from '../actions/types'

export const addFile = file => {
    return{
        type: ADD_FILE,
        payload: file
    }
}

export const addFiles = files => {
    return{
        type: ADD_FILES,
        payload: files
    }
}

export const clearFile = () => {
    return{
        type: CLEAR_FILE
    }
}

export const clearFiles = () => {
    return{
        type: CLEAR_FILES
    }
}

export const clearFileStatus = () => {
    return{
        type: CLEAR_FILE_STATUS
    }
}

export const getProposal = link => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    server.get(link)
}