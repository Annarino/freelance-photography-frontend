import server from '../apis/server';
import history from '../components/global/history';

import{
    CREATE_BID,
    EDIT_BID,
    DELETE_BID,
    FETCH_PROJECT_BIDS,
    FETCH_SUPPLIER_BIDS,
    FETCH_BID,
    SET_SINGLE_BID,
    CLEAR_SINGLE_BID,
    SET_BID_STATUS,
    CLEAR_BID_STATUS,
    SET_BID_VIEW,
    CLEAR_BID_VIEW
}from '../actions/types';

import{ALL, SINGLE, ADD, EDIT, CLEAR, SUCCESS, ERROR, LOADING} from '../actions/types';

export const createBid = (project, proposal) => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    const formData = new FormData()
    const entries = Object.entries(project);
    for(const[key, value] of entries){
        formData.append(key, value)
    }
   
    formData.append('proposal', proposal)
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .post('/bids', formData)
        .then(response => {
            dispatch({
                type:CREATE_BID,
                payload: response.data
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const editBid = formValues => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .put(`/bids/${formValues._id}`, formValues)
        .then(response => {
            dispatch({
                type:EDIT_BID,
                payload: response.data
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const deleteBid = bidId => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .delete(`/bids/${bidId}`)
        .then(response => {
            dispatch({
                type:DELETE_BID,
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const fetchProjectBids = projectId => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .get(`/bids/project/${projectId}`)
        .then(response => {
            dispatch({
                type:FETCH_PROJECT_BIDS,
                payload: response.data
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const fetchSupplierBids = () => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .get('/bids/supplier')
        .then(response => {
            dispatch({
                type:FETCH_SUPPLIER_BIDS,
                payload: response.data
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const fetchBid = bidId => dispatch => {
    server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
    dispatch({
        type:SET_BID_STATUS,
        payload: LOADING
    })
    server
        .post(`/bids/${bidId}`)
        .then(response => {
            dispatch({
                type:FETCH_BID,
                payload: response.data
            })
        })
        .catch(err =>{
            dispatch({
                type: SET_BID_STATUS,
                payload: ERROR
            })
        })
}

export const setSingleBid = bid => {
    return{
        type: SET_SINGLE_BID,
        payload: bid
    }
}

export const clearSingleBid = bid => {
    return{
        type: CLEAR_SINGLE_BID
    }
}

export const setBidStatus = status => {
    return{
        type: SET_BID_STATUS,
        payload: status
    }
}

export const clearBidStatus = () => {
    return{
        type: CLEAR_BID_STATUS
    }
}

export const setBidView = view => {
    return{
        type: SET_BID_VIEW,
        payload: view
    }
}

export const clearBidView = () => {
    return{
        type: CLEAR_BID_VIEW
    }
}