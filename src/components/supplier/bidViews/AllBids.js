import React from 'react';
import {connect} from 'react-redux';

import server from '../../../apis/server'
import ActionModal from '../../global/modals/ActionModal';

import{fetchSupplierBids, setBidView, clearBidView, setSingleBid, deleteBid} from '../../../actions/bidActions';
import {setSingleProject, fetchProject} from '../../../actions/projectActions';

import{MODAL} from '../../../actions/types'

class AllBids extends React.Component{
    constructor(props){
        super(props)
        this.props.fetchSupplierBids();
    }
    componentDidMount(){
        
    }

    renderDeleteBidActions(){
        return( 
            <React.Fragment>
                <div className='ui green button' onClick={() => this.props.editBid({_id:this.props.bid._id, status:'accepted'})}>Accept</div>
                <div className='ui red button' onClick={() => this.props.editBid({_id:this.props.bid._id, status:'rejected'})}>Reject</div>
            </React.Fragment>
            )
    }


    renderDeleteBidModal(){
        if(this.props.view === MODAL && this.props.bid.status === 'accepted'){
            return(
                <ActionModal
                    hideModal={() => this.props.clearBidView()}
                    header="Error"
                    content="You cannot delete a bid once it has been accepted. To cancel an accepted bid, please contact the client and ask them to change the status of your bid to pending or rejected."
                    actions= {<div className='ui cancel button' onClick={() => this.props.clearBidView()}>Close</div>}
                />
            )
        }
        if(this.props.view === MODAL && this.props.bid.status !== 'accepted'){
            return(
                <ActionModal
                    hideModal={() => this.props.clearBidView()}
                    header="Delete Bid"
                    content="Are you sure you want to delete this bid? The client will no longer be able to view your proposal once deleted."
                    actions= {
                        <React.Fragment>
                            <div className='ui cancel button' onClick={() => this.props.clearBidView()}>Cancel</div>
                            <div className='ui red button' onClick={() => this.onDeleteBid()}>Delete</div>
                        </React.Fragment>
                    }
                />
            )
        }
        return
    }

    onDeleteBid(){
        this.props.deleteBid(this.props.bid._id);
        this.props.fetchSupplierBids();
    }

    onDeleteBidClick(bid){
        this.props.setSingleBid(bid)
        this.props.setBidView(MODAL)
    }

    getProposal(slug){
        server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
        server
            .get(slug, {'responseType':'blob'})
            .then(response => {
                const file = new Blob([response.data], {type:'application/pdf'});
                const fileURL = URL.createObjectURL(file);
                window.open(fileURL)
            })
    }

    renderRecords(){
        return this.props.bids.map((bid, index) =>{
            return(
                <tr key={index}>
                    <td data-label="Project Name">{bid.projectId.title}</td>
                    <td data-label="Start Date">{new Date(bid.projectId.startDate).toLocaleDateString()}</td>
                    <td data-label="End Date">{new Date(bid.projectId.endDate).toLocaleDateString()}</td>
                    <td className="selectable" data-label="Proposal" ><a  onClick={() => this.getProposal(bid.proposalURL)} >View Proposal</a></td>
                    <td data-label="Closing Date">TO IMPLEMENT</td>
                    <td data-label="Bid Status">{bid.status}</td>
                    <td className="selectable" data-label="Delete" onClick={() => this.onDeleteBidClick(bid)}><i className="trash alternate outline icon"></i></td>
                </tr>
            )
        })
    }

    renderView(){
        if(this.props.bids.length === 0){
            return(
                <h1>No Bids</h1>
            )
        }
        return(
            <table className="ui celled table">
                {this.renderDeleteBidModal()}
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Proposal</th>
                        <th>Closing Date</th>
                        <th>Bid Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRecords()}
                </tbody>
            </table>
        )
    }
    render(){
        return (this.renderView())
    }
}

const mapStateToProps = state => {
    return{
        bids: Object.values(state.bid.bids),
        bid: state.bid.bid,
        status: state.bid.views.status,
        view: state.bid.views.view,
        project: state.project.singleProject
    }
}

export default connect(mapStateToProps, {fetchSupplierBids, setSingleProject, fetchProject, setBidView, clearBidView, setSingleBid, deleteBid})(AllBids);