import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';

import FileUploader from '../../global/FileUpload'

import {createBid, clearBidStatus} from '../../../actions/bidActions';
import{setProjectView, fetchProjects} from '../../../actions/projectActions';
import {SUCCESS, ERROR} from '../../../actions/types';

import {addFile} from '../../../actions/fileUploadActions';

import Dropzone from 'react-dropzone';

import server from '../../../apis/server'

class NewBid extends React.Component{
    file = null;
 
    onSubmit = () =>{
        if(this.file){
            return this.props.createBid(this.props.project, this.file);
        }
        console.log('file is empty')
    }

    closeModal(){
        this.props.setProjectView('')
        
        this.props.clearBidStatus();
    }

    closeOnSuccess(){
        this.props.clearBidStatus();
        this.props.fetchProjects();
        this.closeModal()
    }

    renderView = () =>{
        console.log(this.props.status)
        if(this.props.status === SUCCESS){
            return(
                <div className="ui dimmer modals page visible active"  onClick={() => this.closeModal()}>
                    <div className="ui longer  modal visible active display-block"  onClick={e => e.stopPropagation()}>
                        <div className="header">Bid Successfully Submitted!</div>
                        <div className="scrolling content">
                        <div className="description">
                        <div className="p-4 pt-0">
                            <h4 className="mt-2">Your bid has been successfully submitted for: <em>{this.props.project.title}.</em></h4>
                            <p>Once the client has reviewed your proposal, they will update the status of your bid.</p>
                            <p>Remember to check your Bids section for updates!</p>
                        </div>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui basic red button" onClick={() => this.closeOnSuccess()}>Close</div>
                    </div>
                    </div>
                </div>
            )
        }
        if(this.props.status === ERROR){
            return(
                <div className="ui dimmer modals page visible active"  onClick={() =>this.closeModal()}>
                    <div className="ui longer  modal visible active display-block"  onClick={e => e.stopPropagation()}>
                        <div className="header">Error</div>
                        <div className="scrolling content">
                        <div className="description">
                        <div className="p-4 pt-0">
                            <h4 className="mt-2">There was an error submitting your bid for "{this.props.project.title}."</h4>
                            <h3>You may have already submitted a bid for this project.</h3>
                        </div>
                        </div>
                    </div>
                    <div className="actions">
                        <div className="ui basic red button" onClick={() => this.closeModal()}>Close</div>
                    </div>
                    </div>
                </div>
            )
        }

        return(
            <div className="ui dimmer modals page visible active"  onClick={() => this.closeModal()}>
                <div className="ui longer  modal visible active display-block"  onClick={e => e.stopPropagation()}>
                    <div className="header">{this.props.project.title}</div>
                    <div className="scrolling content">
                    <div className="description">
                    <div className="p-4 pt-0">
                        <h4 className="mt-2">CLIENT NAME AND LINK HERE</h4>
                        <h4 className="mt-2">{new Date(this.props.project.startDate).toLocaleDateString()} - {new Date(this.props.project.endDate).toLocaleDateString()}</h4>
                        <h4 className="mt-2"><i className="map marker alternate icon"></i>{this.props.project.location}</h4>
                        <h4 className="mt-2">Budget: £{this.props.project.budgetMin} - £{this.props.project.budgetMax}</h4>
                        <p>{this.props.project.description}</p>
                    </div>
                        <form onSubmit={this.props.handleSubmit(this.onSubmit)}  className="ui form error mt-2" encType="multipart/form-data">
                            <div className="field">
                                <label>Proposal: </label>
                                <Dropzone onDrop={acceptedFiles => this.file = acceptedFiles[0]}>
                                    {({getRootProps, getInputProps}) => (
                                        <section>
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <h4>Drag files here.</h4>
                                            <h3>Or click to select a file</h3>
                                        </div>
                                        </section>
                                    )}
                                </Dropzone>
                               
                            </div>
                        </form>
                    </div>
                </div>
                <div className="actions">
                    <div className="ui basic red button" onClick={() => this.closeModal()}>Cancel</div>
                    <div className="ui primary approve button" onClick={this.props.handleSubmit(this.onSubmit)}>Submit Bid</div>
                </div>
                </div>
            </div>
        )
    }
    render(){
        return ReactDOM.createPortal(this.renderView(),document.querySelector('#modal'))
    }
}

const formWrapped = reduxForm({
    form: 'newBid'
})(NewBid)

const mapStateToProps = state => {
    return {
        project: state.project.singleProject,
        view: state.project.views.view,
        status: state.bid.views.status,
        file: state.file.file
    }
}

export default connect(mapStateToProps, {setProjectView, createBid, clearBidStatus, addFile, fetchProjects})(formWrapped);