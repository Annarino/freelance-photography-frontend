import React from 'react';
import {connect} from 'react-redux';

import AllBids from './bidViews/AllBids';
import SingleBid from './bidViews/SingleBid';

import {clearBidStatus} from '../../actions/bidActions';
import {clearProjectStatus} from '../../actions/projectActions'
import{SINGLE} from '../../actions/types'

class Bid extends React.Component{
    componentWillUnmount(){
        this.props.clearBidStatus();
        this.props.clearProjectStatus();
    }
    
    renderView = () =>{
        if(this.props.view === SINGLE){
            return <SingleBid />
        }
        return <AllBids />
    }
    render(){
        return(this.renderView())
    }
}

const mapStateToProps = state => {
    return{
        bids: Object.values(state.bid.bids),
        bid: state.bid.bid,
        view: state.bid.views.view,
        status: state.bid.views.status
    }
}

export default connect(mapStateToProps, {clearBidStatus, clearProjectStatus})(Bid);