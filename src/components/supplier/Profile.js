import React from 'react';
import { connect } from 'react-redux';

import {fetchProfile} from '../../actions';
import SidebarMenu from '../global/SidebarMenu';
import EditProfile from './profileViews/EditProfile';
import Gallery from './Gallery';

class Profile extends React.Component{
    componentDidMount(){
        this.props.fetchProfile();
     }
    renderView = () => {
        switch(this.props.sidebar.willBeActive){
            case "My_Information":
                return <EditProfile initialValues={this.props.profile} />
            case "My_Gallery":
                return <Gallery />
            case "My_Reviews":
                return <div>My reviews to go here</div>
            default:
                return <div> Whoops... Looks like there is an error </div>
        }
    }

    render(){
        return(
            <div className="ui grid">
                <div className="three wide column">
                    <SidebarMenu 
                        menuItems={{
                            My_Information: 'item',
                            My_Gallery: 'item',
                            My_Reviews: 'item'
                            }} 
                    />
                </div>
                <div className="thirteen wide stretched column">
                    <div className="ui segment">
                        {this.renderView()}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
        sidebar: state.sidebar,
        profile: state.profile
    }
}

export default connect(mapStateToProps, { fetchProfile})(Profile);