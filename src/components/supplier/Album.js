import React from 'react';
import { connect } from 'react-redux';

import jsonSamples from '../../apis/jsonSamples';
import PhotoCard from './PhotoCard';
import { 
    setNestedView,
    fetchPhotos,
    clearPhotos 
    } from '../../actions';

class Album extends React.Component{
    componentDidMount () {
        console.log("props",this.props);
        this.props.fetchPhotos(this.props.albumId);
    }

    onAlbumsButtonClick = () => {
        this.props.setNestedView({type: "allAlbums"});
        this.props.clearPhotos();
    }

    renderPhotos = () => {
        if(Object.keys(this.props.photos).length > 0){
            const photos = this.props.photos.map(photo => <PhotoCard title={photo.title} imgSrc={photo.url} />);
            return (
                <React.Fragment>
                <button className="ui teal basic button " onClick={this.onAlbumsButtonClick}>
                            <i className="arrow left icon"></i>
                            Albums
                        </button>
                    <h1 className="ui center aligned dividing header">
                        
                        {this.props.albumName}
                    </h1>
                    <div className = "ui four column grid content">
                        {photos}
                    </div>
                </React.Fragment>
            );
        }
        else{
            return(
                <div className="ui segment">
                    <div className="ui active centered inline loader" />
                </div>
            );
        }
    }

    render(){
        return(
            <div className="ui container">
                {this.renderPhotos()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return(
        { 
        photos: state.photos,
        albumId: state.nestedView.albumId,
        albumName: state.nestedView.albumName
         }
    );
}

export default connect(mapStateToProps, {fetchPhotos, setNestedView, clearPhotos})(Album);