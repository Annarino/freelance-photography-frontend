import React from 'react';
//need to refactor this to make it more generic
//need to change to li class component

class DashboardMenu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        revenue:"active item",
        profits:"item",
        expenses:"item",
        losses:"item",
        bookings:"item"
        };
    }
    
     setActive(item){
        this.setState({[item]: "item active"});
        //console.log(this.state);
        Object.keys(this.state).forEach(key => {
            if(key !== item){
                this.setState({[key]: "item"});
            }
        });
    }

    render(){
        


        console.log(this.props);
        console.log(this.state.revenue);
        return(
            <div className="ui vertical fluid tabular menu">
                    <a className={this.state.revenue} onClick={() => this.setActive("revenue")}>Revenue</a>
                    <a className={this.state.profits} onClick={() => this.setActive("profits")}>Profits</a>
                    <a className={this.state.expenses} onClick={() => this.setActive("expenses")}>Expenses</a>
                    <a className={this.state.losses} onClick={() => this.setActive("losses")}>Losses</a>
                    <a className={this.state.bookings} onClick={() => this.setActive("bookings")}>Bookings</a>
            </div>
        );
    }
}

export default DashboardMenu;