import React from 'react';

import Revenue from './Revenue';
import Profits from './Profits';
import Expenses from './Expenses';


// class DashboardView extends React.Component{
//     render(){
//         return <div><Revenue /></div>;
//     }
// }
const DashboardView = (props) =>{
        switch(props.view){
            case "Revenue":
                return <Revenue />;
            case "Profits":
                return <Profits />;
            case "Expenses":
                return <Expenses />;
            default:
                return <Revenue />;
        
        }
}

export default DashboardView;