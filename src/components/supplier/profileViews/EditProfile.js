import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { editProfile, fetchProfile } from '../../../actions';

class EditProfile extends React.Component {
    

    //Display errors
    renderError({error, touched}){
        if(error && touched){
            return(
                <div className="ui form error">{error}</div>
            );
        }
    }

    //Input field
    renderInput= ({input, value, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <label>{label}</label>
                <input  {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    onSubmit = (formValues) => {
        this.props.editProfile(formValues);
    }

    render(){
        return (
            <React.Fragment>
            <h1 className="ui center aligned dividing header">My Information</h1>
            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                <div className="field">
                    <div className="two fields">
                        <Field  name="firstName" cssClass="required field" label="First Name" component={this.renderInput}/>
                        <Field name="surname" cssClass="required field" label="Surame/Last Name" component={this.renderInput}/>
                    </div>
                    <Field name="companyName" cssClass="field" label="Company Name" component={this.renderInput}/>
                </div>
                <h4 className="ui dividing header">Contact Information</h4>
                    <div className="field">
                        <div className="two fields">
                            <Field name="phoneNumber" cssClass="field" label="Phone Number" component={this.renderInput}/>
                            <Field name="email" cssClass="field" label="Email" component={this.renderInput}/>
                        </div>
                    </div>
                    <Field name="address.streetNumName" cssClass="field" label="Address Line 1" component={this.renderInput}/>
                    <Field name="address.houseNumName" cssClass="field" label="Address Line 2" component={this.renderInput}/>
                    <div className="fields">
                        <Field name="address.city" cssClass="six wide field" label="City" component={this.renderInput}/>
                        <Field name="address.country" cssClass="six wide field" label="Country" component={this.renderInput}/>
                        <Field name="address.postcode" cssClass="four wide field" label="Postcode" component={this.renderInput}/>
                    </div>
                    <button className="ui primary button right floated">Save</button>
            </form>
            </React.Fragment>
        );
    }
}

//Form validation. Return an empty object if everything looks fine
const validate = formValues => {
    const errors = {};
    if (!formValues.firstName ){
        errors.firstName = 'You must enter a first name.';
    }

    if (!formValues.surname){
        errors.surname = 'You must enter a surname/last name.';
    }

    return errors;
}

const formWrapped = reduxForm({
                        form: 'profile',
                        enableReinitialize:true,
                        validate
                    })(EditProfile);

const mapStateToProps = state => {
    return{
        initialValues: state.profile
    }
}
export default connect(mapStateToProps, { editProfile, fetchProfile })(formWrapped);

