import React from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { 
    createInvoice,
    clearNewInvoiceStatus
} from '../../../actions';

class NewInvoice extends React.Component{
    componentWillUnmount(){
        this.props.clearNewInvoiceStatus();
    }

    //Display errors
    renderError({error, touched}){
        if(error && touched){
            return(
                <div className="ui form error">{error}</div>
            );
        }
    }

    //Input field
    renderInput= ({input, placeholder, inputClass, tabIndex, type, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                 <label>{label}</label>
                <input type={type}  tabIndex={tabIndex} placeholder={placeholder} {...input}/>
               
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderCheckbox= ({input, placeholder, inputClass, tabIndex, type, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <input type={type}  tabIndex={tabIndex} placeholder={placeholder} {...input}/>
                <label>{label}</label>
                {this.renderError(meta)}
            </div>
        ); 
    }

    onSubmit = (formValues) => {
       this.props.createInvoice(formValues);
    }

    formClasses(){
        console.log('formClasses')
        if(this.props.addInvoice !== undefined && this.props.addInvoice.status !== undefined && this.props.addInvoice.status === 'loading'){
            return 'ui form loading';
        }
        return 'ui form error'
    }

    renderItems = ({fields, meta: {error}}) => (
        <React.Fragment>
                {fields.map((item, index) => (
                <div className="ui secondary segment">
                    <div className="ui center aligned grid " key={index}>
                        <div className="row">
                            <div className="inline fields">
                                <Field name={`${item}.description`} type="text" cssClass="eight wide field" component={this.renderInput} placeholder="Description" />
                                <Field name={`${item}.VATRate`} type="number" cssClass="four wide field" component={this.renderInput} placeholder="VAT Rate" />
                                <Field name={`${item}.amount`} type="number" cssClass="four wide field" component={this.renderInput} placeholder="Amount excluding VAT" />
                            </div>
                            <div className="middle aligned column primary">
                                <button  type="button" className="ui red button" title="Remove item" onClick={() => fields.remove(index)}><i className="trash alternate outline icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                ))}
            <button type="button" className="ui teal basic right floated button" onClick={() => fields.push({})}><i className="plus icon"></i>Add Item</button>
        </React.Fragment> 
    )

    renderView(){
        if(this.props.addInvoice !== undefined && this.props.addInvoice.status !== undefined){
            if(this.props.addInvoice.status == 200 || this.props.addInvoice.status == 201){
                return(
                    <React.Fragment>
                        <h1>Invoice has been successfully submitted</h1>
                        <button onClick={() => this.props.clearNewInvoiceStatus()}>Add new invoice</button>
                    </React.Fragment>
                );
            }
            if(this.props.addInvoice.status >= 300){
                return(
                    <React.Fragment>
                        <h1>Error: {this.props.addInvoice.status}</h1>
                    </React.Fragment>
                )
            }
        }
        return(
            <React.Fragment>
                <h1 className="ui center aligned header">New Invoice</h1>
                <div className="ui hidden divider" ></div>
                <form onSubmit={this.props.handleSubmit(this.onSubmit)} className={this.formClasses()}>
                    <h4 className="ui dividing header">Your Details</h4>
                    <div className="field">
                        <div className="two fields">
                            <Field  name="supplierInformation.firstName" value={this.props.profile.firstName} cssClass="required field" label="First Name" component={this.renderInput}/>
                            <Field name="supplierInformation.surname" value={this.props.profile.surname} cssClass="required field" label="Surname" component={this.renderInput}/>
                        </div>
                        <Field name="supplierInformation.companyName" cssClass="field" label="Company Name" component={this.renderInput}/>
                        <Field name="supplierInformation.phoneNumber" cssClass="field" label="Phone Number" component={this.renderInput} />
                        <Field name="supplierInformation.email" cssClass="field" label="Email" component={this.renderInput} />
                    </div>
                    <h4 className="ui dividing header">Client Details</h4>
                        <div className="two fields">
                            <Field  name="clientInformation.firstName" value={this.props.profile.firstName} cssClass="required field" label="Contact First Name" component={this.renderInput}/>
                            <Field name="clientInformation.surname" value={this.props.profile.surname} cssClass="required field" label="Contact Surname" component={this.renderInput}/>
                        </div>
                        <Field name="clientInformation.companyName" cssClass="field" label="Company Name" component={this.renderInput}/>
                        <Field name="clientInformation.phoneNumber" cssClass="field" label="Phone Number" component={this.renderInput} />
                        <Field name="clientInformation.email" cssClass="field" label="Email" component={this.renderInput} />
                        <Field name="clientInformation.address.streetNumName" cssClass="field" label="Address Line 1" component={this.renderInput}/>
                        <Field name="clientInformation.address.houseNumName" cssClass="field" label="Address Line 2" component={this.renderInput}/>
                        <div className="fields">
                            <Field name="clientInformation.address.city" cssClass="six wide field" label="City" component={this.renderInput}/>
                            <Field name="clientInformation.address.country" cssClass="six wide field" label="Country" component={this.renderInput}/>
                            <Field name="clientInformation.address.postcode" cssClass="four wide field" label="Postcode" component={this.renderInput}/>
                        </div>
                    <h4 className="ui dividing header">Invoice Details</h4>
                        <Field name="invoiceNumber" cssClass="field" label="Invoice Number (Must be unique. If none supplied, one will be created for you.)" component={this.renderInput} />
                        <div className="three fields">
                            <Field name="supplyDate" cssClass="field" label="Supply Date" type="date" component={this.renderInput} />
                            <Field name="paymentWindow" cssClass="field" label="Payment Window (Days)" type="number" component={this.renderInput} />
                        </div>
                        <div className="two  fields">
                            <div className="inline field">
                                <label>Is the client VAT registered?</label>
                                <Field name="VAT" cssClass="ui checkbox" label="yes" type="checkbox" tabIndex="0" component={this.renderCheckbox} />
                                
                            </div>
                            <Field name="VATNum" type="number" cssClass="inline field" label="VAT Number"  component={this.renderInput} />
                        </div>

                        

                    <h4 className="ui dividing header">Items to Invoice</h4>
                        <div >
                            <FieldArray name="item" component={this.renderItems} />
                        </div>
                    <div className="ui hidden clearing divider"></div>
                    <div className="ui clearing divider"></div>
                    <button className="ui primary button right floated">Save</button>
                </form>
        </React.Fragment>
        )
    }

    render(){
        return(<React.Fragment>{this.renderView()}</React.Fragment>)
    }
}

const formWrapped = reduxForm ({
    form: 'invoices',
    enableReinitialize: true
})(NewInvoice)

const mapStateToProps = state =>{
    let clientInformation = {}
    if(state.invoice.clientInformation !== undefined){
        clientInformation = state.invoice.clientInformation;
    }
    return{
        addInvoice: state.invoice.addInvoice,
        profile: state.profile,
        initialValues: {
            supplierInformation: state.profile,
            clientInformation
        },
    }
}

export default connect(mapStateToProps, {createInvoice, clearNewInvoiceStatus})(formWrapped);