import React from 'react';
import { connect } from 'react-redux';

import {
    fetchInvoices,
    fetchProfile
} from '../../actions';

import SidebarMenu from '../global/SidebarMenu';
import AllInvoices from './invoiceViews/AllInvoices';
import NewInvoice from './invoiceViews/NewInvoice';
import EditInvoice from './invoiceViews/EditInvoice';
import SalesData from './invoiceViews/SalesData';

class Invoices extends React.Component{
    componentDidMount(){
        this.props.fetchProfile();
        //this.props.fetchInvoices();
        if(this.props.invoices === undefined || this.props.invoices.allInvoices === undefined){
            this.props.fetchInvoices();
        }
    }
    renderView = () => {
        switch(this.props.sidebar.willBeActive){
            case "All_Invoices":
                return (
                    <div>
                        <AllInvoices/>
                    </div>
                );
            case "New_Invoice":
                return (
                    <div className="ui very padded secondary segment">
                        <NewInvoice />
                    </div>
                        
                );
            case "Edit_Draft_Invoice":
                return (
                    <div className="ui segment">
                        <EditInvoice />
                    </div>
                );
            case "Sales_Data":
                return (
                    <div className="ui segment">
                        <SalesData />
                    </div>
                );
            default:
                return <div> Whoops... Looks like there is an error </div>
        }
    }

    render(){
        return(
            <div className="ui grid">
                <div className="three wide column">
                    <SidebarMenu 
                        menuItems={{
                            All_Invoices: 'item',
                            New_Invoice: 'item',
                            Sales_Data: 'item'
                            }} 
                    />
                </div>
                <div className="thirteen wide stretched column">
                        {this.renderView()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>{
    return{
        sidebar: state.sidebar,
        profile: state.profile,
        invoices: state.invoices
    }
}

export default connect(mapStateToProps, {fetchProfile, fetchInvoices})(Invoices);