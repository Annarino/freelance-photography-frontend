import React from 'react';
import { connect } from 'react-redux';

import {
    fetchAlbums,
    setNestedView,
    clearNestedView
} from '../../actions';

import Album from './Album';
import AlbumCard from './AlbumCard';

class Gallery extends React.Component{
    componentDidMount(){
        this.props.fetchAlbums(1);
        if(Object.keys(this.props.nestedView).length == 0){
            this.props.setNestedView({type: "allAlbums"});
        }
    }

    renderView = () => {
        if (this.props.nestedView.type === 'allAlbums'){
            if(Object.keys(this.props.albums).length > 0){
                const albums = this.props.albums.map((item,index) => <AlbumCard title={item.title} albumId={item.id} imgSrc="https://semantic-ui.com/images/wireframe/image.png" />);
                return(
                    <React.Fragment>
                        <h1 className="ui center aligned dividing header">
                            My Albums
                        </h1>
                        <div className="ui four column grid">
                            {albums}
                        </div>
                    </React.Fragment>
                    );
            }
        }
        else if(this.props.nestedView.type === 'album'){
            return(<Album />)
        } 
        else{
            return(
                <div className="ui segment">
                    <div className="ui active centered inline loader" />
                </div>
            );
        }
    }

    render(){
        return(
            <div className="ui container">
                {this.renderView()}
            </div>
        );
    };
}

const mapStateToProps = state => {
    return { 
        albums: state.albums,
        nestedView: state.nestedView
        };
}

export default connect(mapStateToProps, {fetchAlbums, setNestedView})(Gallery);