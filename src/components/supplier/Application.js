import React from 'react';
import { Field, FieldArray, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { submitApplication } from '../../actions';

class Application extends React.Component {

    //Display errors
    renderError({error, touched}){
        if(error && touched){
            return(
                <div className="ui form error">{error}</div>
            );
        }
    }

    //Input field
    renderInput= ({input, label, labelWidth, type, placeholder, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <label style={{minWidth: labelWidth}}>{label}</label>
                <input type={type} placeholder={placeholder} {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderTextarea = ({input, type, rows, placeholder, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <textarea type={type} rows={rows} placeholder={placeholder} {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderReferences = ({fields, meta: {error}}) => (
        <React.Fragment>
           
                {fields.map((reference, index) => (
                <div className="ui secondary segment">
                    <div className="ui center aligned grid " key={index}>
                        <div className="row ">
                            <h4 className="four wide column primary">Reference {index + 1}</h4>
                            <div className="middle aligned column primary">
                                <button  type="button" className="ui red button" title="Remove reference" onClick={() => fields.remove(index)}><i className="trash alternate outline icon"></i></button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <Field name={`${reference}.name`} type="text" cssClass="field" component={this.renderInput} placeholder="Full Name" />
                                <div className="two fields">
                                    <Field name={`${reference}.email`} type="text" cssClass="field" component={this.renderInput} placeholder="Email" />
                                    <Field name={`${reference}.phoneNumber`} type="text" cssClass="field" component={this.renderInput} placeholder="Phone Number" />
                                </div>
                                <Field name={`${reference}.notes`} type="text" rows="5" component={this.renderTextarea} placeholder="Notes" />
                            </div>
                        </div>
                    </div>
                </div>
                ))}

            <button type="button" className="ui button" onClick={() => fields.push({})}><i className="plus icon"></i>Add Reference</button>
        </React.Fragment> 
    )

    renderPortfolioLinks = ({fields, meta: {error}}) => (
        <React.Fragment>
                {fields.map((portfolioLinks, index) => (
                <div className="ui secondary segment">
                    <div className="ui center aligned grid " key={index}>
                        <div className="row">
                            <div className=" twelve wide column">
                                <Field name={`${portfolioLinks}.name`} type="text" cssClass="field" component={this.renderInput} placeholder="Add Link" />
                            </div>
                            <div className="middle aligned column primary">
                                <button  type="button" className="ui red button" title="Remove portfolioLinks" onClick={() => fields.remove(index)}><i className="trash alternate outline icon"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                ))}

            <button type="button" className="ui button" onClick={() => fields.push({})}><i className="plus icon"></i>Add Link</button>
        </React.Fragment> 
    )

    onSubmit = (formValues) => {
        this.props.submitApplication(formValues);
    }

    render(){
        return (
            <div className="ui grid ">
                <div className="ui centered row">

                
                <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui fluid form segment error">
                <h1 className="ui center aligned header ">Application</h1>
                <div className="ui section divider"></div>
                    <div className="inline fields">
                        <Field label="Name: " labelWidth="50px" name="firstName" cssClass="seven wide required field" placeholder="First Name" component={this.renderInput}/>
                        <Field name="surname" cssClass="seven wide required field" placeholder="Surname/Last Name" component={this.renderInput}/>
                    </div>
                    <div className="inline fields">
                        <div className="ten wide field">
                            <label>Number of years of experience?</label>
                            <Field name="yearsExperience" type="number" cssClass="required Field" component={this.renderInput} />
                        </div>
                    </div>
                    <h4 className="ui left aligned dividing header">Contact Information</h4>
                        <div className="field">
                            <div className="two fields">
                                <Field name="phoneNumber" cssClass="field" placeholder="Phone Number" component={this.renderInput}/>
                                <Field name="email" cssClass="field" placeholder="Email" component={this.renderInput}/>
                            </div>
                        </div>
                        <Field name="streetNumName" cssClass="field" placeholder="Street Name and Number" component={this.renderInput}/>
                        <Field name="houseNumName" cssClass="field" placeholder="House Name and Number" component={this.renderInput}/>
                        <div className="fields">
                            <Field name="city" cssClass="six wide field" placeholder="City" component={this.renderInput}/>
                            <Field name="country" cssClass="six wide field" placeholder="Country" component={this.renderInput}/>
                            <Field name="postcode" cssClass="four wide field" placeholder="Postcode" component={this.renderInput}/>
                        </div>
                    <h4 className="ui left aligned dividing header">References</h4>
                    <p>References should be past clients. You can add as many references as you would like to help us see how you conduct yourself with clients.</p>
                    <p>You can include any additional information in the notes section.</p>
                    <div >
                        <FieldArray name="references" component={this.renderReferences} />
                    </div>
                    <h4 className="ui left aligned dividing header">Portfolio Links</h4>
                    <p>List any links you may have to any of your online portfolios.</p>
                    <p>(you can also link any of your social media accounts here)</p>
                    <div >
                        <FieldArray name="portfolioLinks" component={this.renderPortfolioLinks} />
                    </div>
                    <div className="ui section divider"></div>
                    <button className="ui large green button center aligned">Submit Application</button>
                </form>
                </div>        
            </div>
        );
    }
}

//Form validation. Return an empty object if everything looks fine
const validate = formValues => {
    const errors = {};
    if (!formValues.firstName ){
        errors.firstName = 'You must enter a first name.';
    }

    if (!formValues.surname){
        errors.surname = 'You must enter a surname/last name.';
    }

    return errors;
}

const formWrapped = reduxForm({
                        form: 'application',
                        validate
                    })(Application);


export default connect(null, { submitApplication })(formWrapped);

