import React from 'react';
import { connect } from 'react-redux';
import Expenses from './dashboardViews/Expenses';
import SidebarMenu from '../global/SidebarMenu';
import Losses from './dashboardViews/Losses';
import Profits from './dashboardViews/Profits';
import Revenue from './dashboardViews/Revenue';
import Bookings from './Bookings';
import DashboardView from './dashboardViews/DashboardView';

class Dashboard extends React.Component {
    renderView = () => {
        switch(this.props.sidebar.willBeActive){
            case "Revenue":
                return <Revenue />
            case "Profits":
                return <Profits />
            case "Expenses":
                return <Expenses />
            case "Losses":
                return <Losses />
            case "Bookings":
                return <Bookings />
            default:
                return <div> Whoops it looks like there was an error!</div>
        }
    }
    render(){
        return( 
            <div className="ui grid">
                <div className="three wide column">
                    <SidebarMenu menuItems={{Revenue: 'item', Profits: 'item', Expenses: 'item', Losses:'item', Bookings:'item'}} selectedItem={this.onSelectedItem}/>
                </div>
                <div className = "thirteen wide stretched column">
                    <div className="ui segment">
                        {this.renderView()}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{ sidebar: state.sidebar}
}

export default connect(mapStateToProps)(Dashboard);