import React from 'react';
import {connect} from 'react-redux';

import NewBid from './bidViews/NewBid';

import {fetchProjects, setSingleProject, setProjectView} from '../../actions/projectActions';
import{ALL, SINGLE, CLEAR, SUCCESS, ERROR, LOADING} from '../../actions/types';

class Project extends React.Component{
    constructor(props){
        super(props);
        this.props.fetchProjects();
    }
    
    renderSingleProject = project => {
        this.props.setSingleProject(project);
        this.props.setProjectView('edit')
    }

    renderBidModal(){
        if(this.props.views === 'BID')
        {
            return(<NewBid />)
        }
        return
    }

    bidButtonClick(project){
        this.props.setSingleProject(project);
        this.props.setProjectView('BID');
    }

    renderView = () => {
        if(this.props.projects.length === 0){
            return(
                <React.Fragment>
                    <h1>No Projects!</h1>
                    <button className="ui button" onClick={this.renderAddProject}>Add Project</button>
                </React.Fragment>
            )
        }
        
        return this.props.projects.map((project, index) => {
            return(
                <React.Fragment key={index}>
                    <div  className="ui raised fluid card" >
                        <div className="content">
                            <div className="header">{project.title}</div>
                        </div>
                        <div className="content">
                            <h4>{new Date(project.startDate).toLocaleDateString()} - {new Date(project.endDate).toLocaleDateString()}</h4>
                            <h4>Budget: £{project.budgetMin} - {project.budgetMax}</h4>
                            <h4>Location: {project.location}</h4>
                            <h4>Description:</h4>
                            <p>{project.description}</p>
                        </div>
                        <div className="ui bottom teal  attached button" onClick={() => this.bidButtonClick(project)}>
                            <i className="add icon"></i>Bid for this project
                        </div>
                    </div>
                </React.Fragment>
                
            )
            })
    }

    render(){
        return(
        <div className={this.props.status==='loading'? "ui active centered inline loader grid":"ui container"}>
            {this.renderBidModal()}
            <div className="row">
                <div className="ui three stackable cards">
                    {this.renderView()}
                </div>
            </div>
        </div>)
    }
}

const mapStateToProps = state => {
    return{
        projects: Object.values(state.project.allProjects),
        project: state.project.singleProject,
        views: state.project.views.view,
        status: state.project.views.status
    }
}


export default connect(mapStateToProps,{fetchProjects, setSingleProject, setProjectView})(Project);