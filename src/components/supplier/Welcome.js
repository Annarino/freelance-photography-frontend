import React from 'react'
import {Link} from 'react-router-dom';

const Welcome = () => {
    return(
        <div>
            <h1>Welcome!</h1>
            <p>Before you get to use all the awesome features of this website, we must first ensure your skills meet our standards</p>
            <p>We maintain a reputation for having only high quality, professional photographers on our site. This benefits not only the photographers, but also the clients searching for photographers.</p>
            <p>To continue, please fill out an application and a member of our staff will contact you in the next few days for a short interview.</p>
            <Link to="/application" className="ui primary basic button">Continue to Application</Link>
        </div>
    )
}

export default Welcome;