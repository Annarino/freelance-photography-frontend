import React from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';

import { setNestedView } from '../../actions';
import Album from './Album';


class AlbumCard extends React.Component{
    renderAlbum = () => {
        this.props.setNestedView({
                type: "album",
                albumId: this.props.albumId,
                albumName: this.props.title
                });
    }

    render(){
        return(
            <div className="column" >
                <div className="ui fluid card" >
                    <div className="ui fluid image">
                        <img src={this.props.imgSrc} onClick={this.renderAlbum}/>
                    </div>
                    <div className="content">
                        <h3 className="header">{this.props.title}</h3>
                    </div>
                </div>
            </div>
           
        );
    }
}
const mapStateToProps = state => {
    return(
        { nestedView: state.nestedView }
    );
}

export default connect(mapStateToProps, {setNestedView})(AlbumCard);