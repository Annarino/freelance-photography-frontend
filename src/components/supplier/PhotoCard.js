import React from 'react';
import {Link} from 'react-router-dom';


class PhotoCard extends React.Component{

    render(){
        return(
            <div className="column" >
                <div className="ui fluid card" >
                    <div className="ui fluid image">
                        <img src={this.props.imgSrc} />
                    </div>
                </div>
            </div>
           
        );
    }
}


export default PhotoCard;