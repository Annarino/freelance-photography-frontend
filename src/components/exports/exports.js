//Global component imports
import SidebarMenu from '../global/SidebarMenu';
import SidebarMenuItem from '../global/SidebarMenuItem';
import Login from '../global/Login';

//Supplier-specific component imports
import SupplierAlbum from '../supplier/Album';
import SupplierAlbumCard from '../supplier/AlbumCard';
import SupplierDashboardBookings from '../supplier/dashboardViews/Bookings';
import SupplierDashboardMenu from '../supplier/dashboardViews/DashboardMenu';
import SupplierDashboardViews from '../supplier/dashboardViews/DashboardView';
import SupplierEditProfile from '../supplier/profileViews/EditProfile';
import SupplierExpenses from '../supplier/dashboardViews/Expenses';
import SupplierGallery from '../supplier/Gallery';
import SupplierLosses from '../supplier/dashboardViews/Losses';
import SupplierProfits from '../supplier/dashboardViews/Profits';
import Revenue from '../supplier/dashboardViews/Revenue';



import Bids_P from '../supplier/Bids';
import Bookings_P from '../supplier/Bookings';
import Dashboard_P from '../supplier/Dashboard';
import Invoices_P from '../supplier/Invoices';
import Profile_P from '../supplier/Profile';

//Client-specific component imports


export {
    GoogleAuth,
    Header,
    history,
    Registration,
    SidebarMenu,
    SidebarMenuItem,
    Login,

    SupplierAlbum,
    AlbumCard_P,
    Bids_P,
    Bookings_P,
    Dashboard_P,
    SupplierDashboardBookings,
    SupplierDashboardMenu,
    SupplierDashboardViews,
    SupplierEditProfile,
    SupplierExpenses,
    Gallery_P,
    Invoices_P,
    SupplierLosses,
    Profile_P,
    Profits_P,
    Revenue_P
   
}