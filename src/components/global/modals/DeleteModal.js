import React from 'react';
import ReactDOM from 'react-dom'

const DeleteModal = props => {
    return ReactDOM.createPortal((
        <div className="ui dimmer modals page visible active" onClick={props.hideModal}>
            <div className="ui mini modal visible active" onClick = {e=>e.stopPropagation}>
                <div className="header">{props.header}</div>
                <div className="content">{props.content}</div>
                <div className="actions">{props.actions}</div>
            </div>
        </div>
        ),
        document.querySelector('#modal')
    )
}

export default DeleteModal;