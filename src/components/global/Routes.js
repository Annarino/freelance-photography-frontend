import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import ErrorPage from '../global/ErrorPage';
import SupplierWelcome from '../supplier/Welcome';
import SupplierApplication from '../supplier/Application';
import SupplierAlbum from '../supplier/Album';
import SupplierBids from '../supplier/Bid';
import SupplierDashboard from '../supplier/Dashboard';
import SupplierInvoices from '../supplier/Invoices';
import SupplierBookings from '../supplier/Bookings';
import SupplierProfile from '../supplier/Profile';
import SupplierEditProfile from '../supplier/profileViews/EditProfile';
import SupplierProject from '../supplier/Project';

import PendingSupplierDashboard from '../supplier/pendingComponents/Dashboard';
import PendingSupplierInvoices from '../supplier/pendingComponents/Invoice';
import PendingSupplierBookings from '../supplier/pendingComponents/Bookings';
import PendingSupplierBids from '../supplier/pendingComponents/Bids';

import ClientHeader from '../client/Header';
import ClientInvoice from '../client/Invoice';
import ClientProject from '../client/Project';
import ClientProfile from '../client/Profile';
import ClientDashboard from '../client/Dashboard';
import ClientAddProject from '../client/projectViews/NewProject';
import ClientSupplier from '../client/Supplier';

import history from '../global/history';
import Header from '../global/Header';
import Login from '../global/Login';
import Registration from '../global/Registration';

import { fetchSupplierApplication } from '../../actions'

class Routes extends React.Component{
    renderSupplierAcceptedRoutes(){
        return(
            <Router history={history}>

                <Header />
                <div className="ui container">
                    <Switch>
                        <Route path='/dashboard' exact component={SupplierDashboard} />
                        <Route path='/bids' exact  component={SupplierBids} />
                        <Route path='/bookings' exact component={SupplierBookings} />
                        <Route path='/invoices' exact component={SupplierInvoices} />
                        <Route path='/my-profile' exact component = {SupplierProfile} />
                        <Route path='/register' exact component = {Registration} />
                        <Route path='/albums/:id' exact component = {SupplierAlbum} />
                        <Route path='/projects' exact component = {SupplierProject} />
                    </Switch>
                        
                </div>
            </Router>
        );
    }

    renderSupplierPendingRoutes(){
        console.log('render supplier pending')
        return(
            <Router history={history}>
                <Header />
                <div className="ui container">
                    <Switch>
                        <Route path='/welcome' exact component = {SupplierWelcome} />
                        <Route path='/application' exact component = {SupplierApplication} />
                        <Route path='/dashboard' exact component={PendingSupplierDashboard} />
                        <Route path='/bids' exact  component={PendingSupplierBids} />
                        <Route path='/bookings' exact component={PendingSupplierBookings} />
                        <Route path='/invoices' exact component={PendingSupplierInvoices} />
                        <Route path='*' component = {ErrorPage} />
                    </Switch>
                </div>
            </Router>
        )
    }

    renderClientRoutes(){
        return(
            <Router history = {history}>
                <ClientHeader />
                <div className="ui container">
                    <Switch>
                        <Route path='/dashboard' exact component={ClientDashboard} />
                        <Route path='/projects' exact  component={ClientProject} />
                        <Route path='/invoices' exact component={ClientInvoice} />
                        <Route path ='/my-profile' exact component={ClientProfile} />
                        <Route path = '/projects/new-project' exact component = {ClientAddProject} />
                        <Route path = '/photographers' exact component = {ClientSupplier} />
                        <Route path='*' component = {ErrorPage} />
                    </Switch>
                </div>
            </Router>
        )
    }
    
    renderLoginRoutes(){
        return(
            <Router history={history}>
                <Header />
                <div className="ui container">
                    <Switch>
                        <Route path='/login' exact component ={ Login } />
                        <Route path='/register' exact component = {Registration} />
                        <Route path='/' exact component ={ Login } />
                        <Route path='*' component = {ErrorPage} />
                    </Switch>
                </div>
            </Router>
        );
    }
    
    renderView(){
        if(this.props.isSignedIn){
            if(this.props.userType === 'supplier' && this.props.userStatus === 'accepted'){
                return this.renderSupplierAcceptedRoutes();
            }
            else if(this.props.userType === 'supplier' && this.props.userStatus === 'pending'){
                return this.renderSupplierPendingRoutes();
                // fetchSupplierApplication();
                // switch(this.props.applicationStatus){
                //     case 'not_submitted':
                //         console.log('not submitted')
                //         return (<h1>Not submitted</h1>);
                //     case 'denied':
                //         return(<h1>Application denied</h1>);
                //     default :
                //         return(<h1>Application pending </h1>);
                // }
            } else if(this.props.userType === 'client'){
                return this.renderClientRoutes();
            }else{
                return this.renderLoginRoutes();
            }
        }
        console.log('default')
        return this.renderLoginRoutes();
    }
    
    render(){
        return(
        <div>
            {this.renderView()}
        </div>
        );
    }
}

const mapStateToProps = state => {
    return { isSignedIn: state.auth.isSignedIn, userType: state.auth.userType, userStatus: state.auth.userStatus, applicationStatus: state.application.status};
}

export default connect(mapStateToProps, {fetchSupplierApplication})(Routes);