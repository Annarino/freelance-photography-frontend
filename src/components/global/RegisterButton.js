import React from 'react';

import {Link} from 'react-router-dom'
import history from './history';

const RegisterButton = () => {
    return <Link className="ui inverted button" to='/register'>Sign Up</Link>
}

export default RegisterButton;