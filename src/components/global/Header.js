import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import AuthButton from './AuthButton';
import RegisterButton from './RegisterButton';


class Header extends React.Component{

    renderHeaderView(isSignedIn){
        if (isSignedIn){ 
        return(
            <div style={{padding: '10px 20px'}} className="ui secondary menu">
                <NavLink activeClassName="active" to="/dashboard" className="item">Dashboard</NavLink>
                <NavLink activeClassName="active" to="/Invoices" className="item">Invoices</NavLink>
                <NavLink activeClassName="active" to="/bookings" className="item">Bookings</NavLink>
                <NavLink activeClassName="active" to="/bids" className="item">Bids</NavLink>
                <NavLink activeClassName="active" to="/projects" className="item">Browse Projects</NavLink>
                <div className="right menu">
                    <NavLink activeClassName="active" to="/my-profile" className="item">
                    <i className="user icon big" ></i>
                    </NavLink>
                    <AuthButton />
                </div>
                
            </div>
            );
        }
        else{
            return(
                <div style={{padding: '10px 20px'}} className="ui teal inverted secondary menu">
                    <div className="right menu">
                        <RegisterButton />
                        <AuthButton />
                    </div>
                </div>
            );
        }
    }
    render(){
        return (
            <React.Fragment>
                {this.renderHeaderView(this.props.isSignedIn)} 
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {isSignedIn: state.auth.isSignedIn};
}

export default connect(mapStateToProps)(Header);