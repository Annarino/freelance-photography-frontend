import React from 'react';
import {connect} from 'react-redux';
import history from './history';
import {signIn, signOut} from '../../actions'

class AuthButton extends React.Component{

    onSignInClick = () => {
        history.push('/login');
    }

    onSignOutClick = () => {
        console.log('sign out clicked')
        this.props.signOut();
    }

    renderAuthButton(props){
        console.log('Button: '+this.props.isSignedIn)
        switch(this.props.isSignedIn){
            case true:
                return <button className="ui red button" onClick={this.onSignOutClick}>Sign out</button>
            case false:
                return <button className="ui green button" onClick={this.onSignInClick}>Login</button>
            default:
               return <button className="ui green button" onClick={this.onSignInClick}>Login</button>;
        }
    }


    render(){
        return (
            <React.Fragment>
                {this.renderAuthButton(this.props)}
            </React.Fragment>
            );
    }
}

const mapStateToProps = state => {
return {isSignedIn: state.auth.isSignedIn};
}

export default connect(mapStateToProps, {signIn, signOut})(AuthButton);