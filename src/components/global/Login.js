import React from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import{ signIn } from '../../actions';

import history from '../global/history';


class Login extends React.Component{
    onSubmit = (formValues) => {
        this.props.signIn(formValues);
    }

    renderError( name, {touched, error} ){
        if(touched && error){
            return (
                <div className="ui error message">
                    <p>
                        {error}
                    </p>
                </div>
            );   
        }
    }

    renderFields = ({input, label, type, meta}) => {
        return (
            <div className={this.renderError(input.name, meta) ? 'field required error' : 'field required'}>
                <label>{label}</label>
                <input type={type} {...input} />
                { this.renderError(input.name, meta) }
            </div>
        );
    }
    
    render(){
        return (
            <div className="ui grid">
                <div className="centered row">
                    <h1 className="ui center aligned header">Log in to your account.</h1>
                </div>
                <div className="centered row">
                    <div className="ui secondary very padded compact left aligned segment">
                        <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                            <Field name="email" label="Email" type="email" component={this.renderFields} />
                            <Field name="password" type="password" label="Password" component={this.renderFields} />
                            <button className="ui fluid button teal large" onSubmit={this.validate}>Log In</button>
                        </form>
                        <h3 className="ui center aligned header">Not a member? Create and account <Link to="/register">here</Link>.</h3>
                    </div>
                </div>
                
            </div> 
        );
    }
}

const validate = formVals => {
    const errors = {};
    if(!formVals.email){
        errors.email = 'You must enter an email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formVals.email)){
        errors.email = 'Please enter a valid email'
    }
    if(!formVals.password){
        errors.password = 'You must enter a password'
    }
    return errors;
}

const formWrapped = reduxForm({
    form: 'login',
    validate : validate
})(Login);

const mapStateToProps = state => {
    return{ isSignedIn: state.isSignedIn};
}

export default connect(mapStateToProps, {signIn})(formWrapped);