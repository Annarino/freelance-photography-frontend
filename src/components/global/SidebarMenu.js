import React from 'react';
import { connect } from 'react-redux';

import { createSidebar, updateSidebar, clearSidebar } from '../../actions';

class SidebarMenu extends React.Component{
    componentDidMount(){
        this.props.createSidebar(this.props.menuItems, this.props.wasActive);
    }

    setActiveItem(item){
        this.props.updateSidebar(this.props.sidebar.willBeActive, item);
    }

    componentWillUnmount(){
        this.props.clearSidebar();
    }

    renderList = () => {
        return Object.keys(this.props.menuItems).map((item, index) => <a key={index} className={this.props.sidebar[item]}  onClick={() => this.setActiveItem(item)}>{item.split("_").join(" ")}</a>);
    }

    render(){
        return( 
            <div className="ui vertical fluid tabular menu">
                {this.renderList()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {sidebar: state.sidebar}
}

export default connect(mapStateToProps, { createSidebar, updateSidebar, clearSidebar })(SidebarMenu);

// class SidebarMenu extends React.Component{
//     constructor(props){
//         super(props);
//         this.state={};
//         this.props.menuItems.forEach((item, index) => {
//                 if(index === 0){
//                     this.state[item] = "active item";
//                     this.props.selectedItem(item);
//                 }
//                 else{
//                     this.state[item] = "item";
//                 }
//             } 
//         );
//     };

//      setActive(item){
//         this.setState({[item]: "item active"});
//         this.props.selectedItem(item);
//         Object.keys(this.state).forEach(key => {
//             if(key !== item){
//                 this.setState({[key]: "item"});
//             }
//         });
//     }

//     renderList(classes){
//         return this.props.menuItems.map( item => {
//                 return(
//                     <a className={this.state[item]} onClick={() => this.setActive(item)}>{item}</a>
//                 );
//             });
//     }

//     render(){
//         return( 
//             <div className="ui vertical fluid tabular menu">
//                 {this.renderList()}
//             </div>
//         );
//     }
// }

// export default SidebarMenu;