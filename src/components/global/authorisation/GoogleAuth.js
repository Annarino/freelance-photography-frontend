import React from 'react';
import { connect } from 'react-redux';
import history from '../../../components/global/history';
import { signIn, signOut, createUser, fetchUser, onAuthChange } from '../../../actions';

class GoogleAuth extends React.Component{

    componentDidMount(){
        window.gapi.load('client:auth2', ()=>{
            window.gapi.client.init({
                clientId: '216561190679-ve035apgcqa9btm73tb05p33klpi6e70.apps.googleusercontent.com',
                scope: 'User'
            }).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                //this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange);
            });
        });
    }

    onAuthChange = (isSignedIn) => {
        console.log(this.props);
        if(isSignedIn){
            //this.props.signIn(this.auth.currentUser.get().getId());
            // if(this.props.newUser){
            //     this.props.createUser('google', this.auth.currentUser.get().getId(), null)
            // }
            // else{
            //     history.push('/dashboard');
            // }
        }
        else{
            this.props.signOut();
        }
    };

    onSignInClick = () => {
        this.props.signIn();
    }

    onSignOutClick = () => {
        this.props.signOut();
    }

    renderAuthButton(props){
        switch(this.props.isSignedIn){
            case true:
                return <button className="ui red google button" onClick={this.onSignOutClick}>Sign out</button>
            
            case false:
                return <button className="ui green google button" onClick={this.onSignInClick}><i className="google icon" />Log in with Google</button>

            default:
               return <button className="ui green google button" onClick={this.onSignInClick}><i className="google icon" />Log in with Google</button>;

        }
    }

    renderCreateUserButton(props){
        
        switch(this.props.isSignedIn){
            case null:
                return <button className="ui green google button" onClick={this.onSignInClick} ><i className="google icon" />Log in with Google</button>;
            case false:
                return <button className="ui green google button" onClick={this.onSignInClick} ><i className="google icon" />Log in with Google</button>;
            case true:
                return <div> You are already signed in with a Google account </div>;
            default:
                return <div>Looks like something went wrong </div>;
        }
    }

    render(){
        return (
            <React.Fragment>
            {this.renderAuthButton(this.props)}
            </React.Fragment>
            );
    }
}

const mapStateToProps = state => {
    return {isSignedIn: state.auth.isSignedIn};
}

export default connect(mapStateToProps, {signIn, signOut, fetchUser, createUser})(GoogleAuth);