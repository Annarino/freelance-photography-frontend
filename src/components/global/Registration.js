import React from 'react';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { createUser } from "../../actions";


class Registration extends React.Component{
    onSubmit = (formValues) => {
        this.props.createUser('basic',null,formValues);
    }

    renderError(name, {touched, error}){
        if(touched && error){
            return(
                <div className="ui error message">
                    <p>{error}</p>
                </div>
            );
        }
        if(error && name==="passwordConfirm"){
            return(
                <div className="ui error message">
                    <p>{error}</p>
                </div>
            );
        }
    }

    renderFields = ({input, label, type, meta}) => {
        return (
            <div className="field required">
                <label>{label}</label>
                <input  type={type} {...input} />
                {this.renderError(input.name, meta)}
            </div>
        );
    }

    renderRadioFields({input, label, type, tabIndex, value}){
        return(
            <div className="field">
                <div className="ui radio checkbox">
                    <input   type={type} tabIndex={tabIndex} value={value} {...input} />
                    <label>{label}</label>
                </div>
            </div>
        );
    }
    
    render(){
        return (
         
            <div className="ui grid">
                <div className="centered row">
                    <h1 className="ui center aligned header">Create a new account.</h1>
                </div>
                <div className="centered row">
                    <div className="ui very padded left aligned compact secondary segment">
                        <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error" >
                            <Field name="email" type="email" label="Email" component={this.renderFields} />
                            <Field name="password" type="password" label="Password" component={this.renderFields} />
                            <Field name="passwordConfirm" type="password" label="Confirm Password" component={this.renderFields} />
                            <div className="ui padded grouped fields required">
                                <label className="field" for="userType">You are:</label>
                                <Field name="userType" type="radio" label="A professional photographer." tabIndex="0" value="supplier" component={this.renderRadioFields} />
                                <Field name="userType" type="radio" label="A client searching for professional photographers." tabIndex="1" value="client" component={this.renderRadioFields} />
                            </div>
                            <button className="ui fluid button teal large" >Create an Account</button>
                        </form>
                        <h3 className="ui center aligned header">Already have an account? log in <Link to="/login">here</Link>.</h3>
                    </div>
                </div>
                
            </div>
        );
    }
}

const validate = formVals => {
    const errors = {};
    if(!formVals.email){
        errors.email = 'You must enter an email'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formVals.email)){
        errors.email = 'Please enter a valid email'
    }
    if(!formVals.password){
        errors.password = 'You must enter a password'
    }

    if(formVals.password !== formVals.passwordConfirm){
        errors.passwordConfirm = 'Passwords do not match!'
    }
    return errors;
}

const formWrapped = reduxForm({
    form: 'registration',
    validate
    })(Registration);


export default connect(null, { createUser })(formWrapped);