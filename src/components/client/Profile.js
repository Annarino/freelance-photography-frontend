import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { editProfile, fetchProfile } from '../../actions';

class EditProfile extends React.Component {
    componentDidMount(){
        this.props.fetchProfile();
     }

    //Display errors
    renderError({error, touched}){
        if(error && touched){
            return(
                <div className="ui form error">{error}</div>
            );
        }
    }

    renderInput= ({input, value, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <label>{label}</label>
                <input  {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderCheckbox= ({input, placeholder, inputClass, tabIndex, type, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <input type={type}  tabIndex={tabIndex} placeholder={placeholder} {...input}/>
                <label>{label}</label>
                {this.renderError(meta)}
            </div>
        ); 
    }

    onSubmit = (formValues) => {
        this.props.editProfile(formValues);
    }

    render(){
        return (
            <div className="ui very padded compact container segment">
            <h1 className="ui center aligned dividing header">My Information</h1>
            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form error">
                <div className="p-4">
                    <div className="field">
                        <div className="two fields">
                            <Field  name="firstName" cssClass="required field" label="First Name" component={this.renderInput}/>
                            <Field name="surname" cssClass="required field" label="Surame/Last Name" component={this.renderInput}/>
                        </div>
                        <Field name="companyName" cssClass="field" label="Company Name" component={this.renderInput}/>
                    </div>
                </div>
                <h3 className="ui dividing header mt-4">Contact Information</h3>
                <div className="p-4">
                    <div className="field">
                            <div className="two fields">
                                <Field name="phoneNumber" cssClass="field" label="Phone Number" component={this.renderInput}/>
                                <Field name="email" cssClass="field" label="Email" component={this.renderInput}/>
                            </div>
                        </div>
                        <Field name="address.streetNumName" cssClass="field" label="Address Line 1" component={this.renderInput}/>
                        <Field name="address.houseNumName" cssClass="field" label="Address Line 2" component={this.renderInput}/>
                        <div className="fields">
                            <Field name="address.city" cssClass="six wide field" label="City" component={this.renderInput}/>
                            <Field name="address.country" cssClass="six wide field" label="Country" component={this.renderInput}/>
                            <Field name="address.postcode" cssClass="four wide field" label="Postcode" component={this.renderInput}/>
                    </div>
                </div>
                <h3 className="ui dividing header mt-4">VAT Information</h3>
                <div className="p-4">
                    <p>We use this information to make invoicing easier for photographers. This will only be visible for photographers who have been accepted to a project you have posted.</p>
                    <Field name="VATNum" type="number" cssClass="inline field" label="VAT Number"  component={this.renderInput} />
                </div>
                <button className="ui primary button right floated">Save</button>
            </form>
            </div>
        );
    }
}

//Form validation. Return an empty object if everything looks fine
const validate = formValues => {
    const errors = {};
    if (!formValues.firstName ){
        errors.firstName = 'You must enter a first name.';
    }

    if (!formValues.surname){
        errors.surname = 'You must enter a surname/last name.';
    }

    return errors;
}

const formWrapped = reduxForm({
                        form: 'profile',
                        enableReinitialize:true,
                        validate
                    })(EditProfile);

const mapStateToProps = state => {
    return{
        initialValues: state.profile
    }
}
export default connect(mapStateToProps, { editProfile, fetchProfile })(formWrapped);

