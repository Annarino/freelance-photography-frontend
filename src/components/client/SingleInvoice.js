import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import {clearSingleInvoiceView, editInvoice, clearEditSuccess} from '../../actions';


class SingleInvoice extends React.Component{
    componentWillUnmount(){
        this.props.clearEditSuccess();
    }

    renderItemizedList(){
        return (this.props.invoice.singleInvoice.item.map((item, index) => {
            return(
                <tr key={index}>
                <td>{item.description}</td>
                <td>{item.VATRate}</td>
                <td>{item.amount}</td>
            </tr>
            )
        })
        )
    }

    renderAllInvoices(){
        this.props.clearSingleInvoiceView();
    }

    renderEditSuccess(){
        if(this.props.invoice.views.editSuccess.trim() === '' ){
            return;
        }
        if(this.props.invoice.views.editSuccess ){
            return(
                <div class="ui success message">
                    <div class="header">Invoice Updated!</div>
                </div>
            )
        }
        if(!this.props.invoice.views.editSuccess){
            return(
                <div class="ui error message">
                    <div class="header">There was an error updating the invoice.</div>
                </div>
            );
        }
        return;
    }

    renderVATNum(){
        if(this.props.invoice.singleInvoice.VATNum !== undefined && this.props.invoice.singleInvoice !== ''){
            return <h4 className="color-black">VAT Number: {this.props.invoice.singleInvoice.VATNum}</h4>
        }
        return
    }

    statusSelected(value){
        if(this.props.status === value){
            return true
        }
        return false
    }
    
    calculatSubTotal(){
        let subtotal = 0.00;
        this.props.invoice.singleInvoice.item.forEach(item => {
            subtotal += parseFloat(item.amount);
        })
        return subtotal.toFixed(2);
    }

    calculatVATTotal(){
        let VATTotal = 0.00;
        this.props.invoice.singleInvoice.item.forEach(item => {
            VATTotal += ((parseFloat(item.VATRate)/100)*parseFloat(item.amount));
        })
        return VATTotal.toFixed(2);
    }

    calculateTotalDue(){
        return (parseFloat(this.calculatSubTotal()) + parseFloat(this.calculatVATTotal())).toFixed(2);
    }

    onSubmit = formValues => {
        this.props.editInvoice(formValues, this.props.invoice.singleInvoice._id)
    }

    pastDue(){
        let pastDue = new Date(this.props.invoice.singleInvoice.issueDate);
        pastDue.setDate(pastDue.getDate() + this.props.invoice.singleInvoice.paymentWindow)
        pastDue = pastDue.toDateString();
        return pastDue;
    }

    render(){
        return(
        <div className="ui container p-4">
            <button className="ui basic teal button" onClick={() => this.renderAllInvoices()}><i class="arrow left icon"></i> All Invoices</button>
            <div className="ui very padded clearing compact container segment">
                <div className="ui two column grid">
                    {this.renderEditSuccess()}
                    <div className="row">
                        <div className="six wide right floated column">
                            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="ui form">
                                <div class="ui inline fields">
                                    <label>Status</label>
                                    <Field className="ui inline dropdown"  name="status" component="select">
                                        <option value="unpaid">Unpaid</option>
                                        <option  value="paid">Paid</option>
                                    </Field>
                                    <button className="ui green right floated button">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="row">
                        <div className="column ">
                        <h2>Issued By:</h2>
                            <div className="ml-3">
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.supplierInformation. firstName + " " + this.props.invoice.singleInvoice.supplierInformation. surname}</h4>
                                {() => {
                                    if(this.props.invoice.singleInvoice.supplierInformation.companyName !== undefined){
                                        return <h4>{this.props.invoice.singleInvoice.supplierInformation.companyName}</h4>
                                    }
                                    return;
                                }}
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.supplierInformation.phoneNumber}</h4>
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.supplierInformation.email}</h4>
                            </div>
                            <div className="ui divider"></div>
                            <h2>Issued To:</h2>
                            <div className="ml-3">
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.clientInformation. firstName + " " + this.props.invoice.singleInvoice.clientInformation. surname}</h4>
                                {() => {
                                    if(this.props.invoice.singleInvoice.clientInformation.companyName !== undefined){
                                        return <h4 className="mt-0">Company: <span className="text-light">{this.props.invoice.singleInvoice.clientInformation.companyName}</span></h4>
                                    }
                                    return;
                                }}
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.clientInformation.phoneNumber}</h4>
                                <h4 className="mt-0">{this.props.invoice.singleInvoice.clientInformation.email}</h4>
                                <h3 className="mt-1">Address: </h3>
                                <div className="ml-3">
                                    <h4 className="mt-0 text-light">{this.props.invoice.singleInvoice.clientInformation.address.streetNumName}</h4>
                                    <h4 className="mt-0 text-light">{this.props.invoice.singleInvoice.clientInformation.address.houseNumName}</h4>
                                    <h4 className="mt-0 text-light">{this.props.invoice.singleInvoice.clientInformation.address.city}</h4>
                                    {() => {
                                        if(this.props.invoice.singleInvoice.clientInformation.address.province !== undefined){
                                        return <h4 className="mt-0 text-light">{this.props.invoice.singleInvoice.clientInformation.address.province}</h4>
                                        }
                                        return
                                        }}
                                    <h4 className="mt-0 text-light">{this.props.invoice.singleInvoice.clientInformation.address.country}</h4>
                                </div>
                            </div>
                        </div>
                        <div className="column ">
                            <div className="ui very padded secondary segment">
                                <h2 className="color-black">Invoice Details</h2>
                                <div className="ml-3">
                                    <h4 className="color-black">Invoice Number: {this.props.invoice.singleInvoice.invoiceNumber ? this.props.invoice.singleInvoice.invoiceNumber : this.props.invoice.singleInvoice._id}</h4>
                                    <h4 className="color-black">Supply Date: {new Date(this.props.invoice.singleInvoice.supplyDate).toDateString()}</h4>
                                    <h4 className="color-black">Invoice Date:  {new Date(this.props.invoice.singleInvoice.issueDate).toDateString()}</h4>
                                    <h4 className="color-black">Payment Window:  {this.props.invoice.singleInvoice.paymentWindow} Days</h4>
                                    <h4 className="color-black">Due Date: {this.pastDue()}</h4>
                                    {this.renderVATNum()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ui hidden divider"></div>
                <div>
                    <table className="ui top attached celled table">
                        <thead>
                            <tr>
                                <th>Description</th>
                                <th>VAT Rate(%)</th>
                                <th>Amount(£)</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderItemizedList()}
                        </tbody>
                        
                    </table>
                    <table className="ui attached right aligned definition collapsing table float-right">
                        <tbody>
                            <tr>
                                <td>Subtotal: </td>
                                <td>{this.calculatSubTotal()}</td>
                            </tr>
                            <tr>
                                <td>VAT Charged(£): </td>
                                <td>{this.calculatVATTotal()}</td>
                            </tr>
                            <tr>
                                <td>Total Due: </td>
                                <td>{this.calculateTotalDue()}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div >
        )
    }
}

const formWrapped = reduxForm({
    form:'updateStatus',
    enableReinitialization: true
})(SingleInvoice)

const mapStateToProps = state=> {
    return{
        invoice: state.invoice,
        initialValues: state.invoice.singleInvoice
    }
}

export default connect(mapStateToProps, {clearSingleInvoiceView, editInvoice, clearEditSuccess})(formWrapped)