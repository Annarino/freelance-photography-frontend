import React from 'react';
import {connect} from 'react-redux';

import AllProjects from './projectViews/AllProjects';
import SingleProject from './projectViews/SingleProject';
import EditProject from './projectViews/EditProject';
import NewProject from './projectViews/NewProject';

import{fetchUsersProjects, clearProjectStatus} from '../../actions/projectActions'
import{SINGLE, ADD, EDIT} from '../../actions/types';

class Project extends React.Component{
   

    componentWillUnmount(){
        this.props.clearProjectStatus();
    }

    renderView = () => {
        switch(this.props.views){
            case SINGLE:
                return <SingleProject />
            case EDIT:
                return <EditProject />
            case ADD:
                return <NewProject />
            default:
                return <AllProjects />
        }
    }

    render(){
        return(
            <React.Fragment>
                {this.renderView()}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return{
        projects: state.project.allProjects,
        project: state.project.singleProject,
        views: state.project.views.view,
        status: state.project.views.status
    }
}

export default connect(mapStateToProps, {fetchUsersProjects, clearProjectStatus})(Project)