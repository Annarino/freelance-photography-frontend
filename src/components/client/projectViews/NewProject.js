import React from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';

import {setProjectView, createProject, clearProjectStatus} from '../../../actions/projectActions';
import{ALL, SINGLE, ADD, EDIT, CLEAR, SUCCESS, ERROR, LOADING} from '../../../actions/types';

class NewProject extends React.Component{
    componentWillUnmount(){
        this.props.clearProjectStatus();
    }

    //Display errors
    renderError({error, touched}){
        if(error && touched){
            return(
                <div className="ui form error">{error}</div>
            );
        }
    }

    renderInput= ({input, placeholder, tabIndex, type, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                 <label>{label}</label>
                <input type={type}  tabIndex={tabIndex} placeholder={placeholder} {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderTextarea = ({input, type, rows, placeholder, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <textarea type={type} rows={rows} placeholder={placeholder} {...input}/>
                {this.renderError(meta)}
            </div>
        ); 
    }

    renderCheckbox= ({input, placeholder, inputClass, tabIndex, type, label, cssClass, meta}) => {
        return(
            <div className={ this.renderError(meta) ? cssClass+" error" : cssClass}>
                <input type={type}  tabIndex={tabIndex} placeholder={placeholder} {...input}/>
                <label>{label}</label>
                {this.renderError(meta)}
            </div>
        ); 
    }

    onPostButtonSubmit = (formValues) => {
        formValues.status = 'open';
        this.props.createProject(formValues);
    }

    onDraftButtonSubmit = (formValues) => {
        formValues.status = 'draft';
        this.props.createProject(formValues);
    }

    formClasses(){
        if(this.props.status === LOADING){
            return 'ui form loading';
        }
        return 'ui form error'
    }

    renderView(){
        
        if(this.props.status === SUCCESS){
            return(
                <React.Fragment>
                    <h1>Project has been posted!</h1>
                    <button onClick={() => this.props.clearProjectStatus()}>Add new invoice</button>
                </React.Fragment>
            );
        }
        if(this.props.status === ERROR){
            return(
                <React.Fragment>
                    <h1>Error: {this.props.status}</h1>
                </React.Fragment>
            )
        }
        return(
        <React.Fragment>
                <h1 className="ui center aligned header">New Project</h1>
                <form  className={this.formClasses()}>
                    <div className="inline fields">
                        <label>Title: </label>
                        <Field name="title" type="text" cssClass="required sixteen wide field" component={this.renderInput}/>
                    </div>
                    <div className="inline fields">
                        <label>Dates:</label>
                        <Field name="startDate" type="date" placeholder="Start" cssClass="required field" component={this.renderInput}/>
                        <Field name="endDate" type="date" placeholder="End" cssClass="required field" component={this.renderInput}/>
                    </div>
                    <div className="inline fields">
                        <Field name="location" type="text" label="Location:" cssClass="sixteen wide field" component={this.renderInput}/>
                    </div>
                    <div className="inline fields">
                        <label>Budget:</label>
                        <Field name="budgetMin" placeholder="Min" type="number" cssClass="field" component={this.renderInput} />
                        <label>-</label>
                        <Field name="budgetMax" placeholder="Max" type="number" cssClass="field" component={this.renderInput} />
                    </div>
                    <div className="field">
                        <label>Project Description:</label>
                        <Field name="description" type="text" rows="5" component={this.renderTextarea} />
                    </div>
                    <div className="two fields">
                        <div className="field">
                            <button type="submit" onClick={this.props.handleSubmit(this.onPostButtonSubmit)} className="ui fluid green button">Post Project</button>
                        </div>
                        <div className="field">
                        <button type="submit" onClick={this.props.handleSubmit(this.onDraftButtonSubmit)} className="ui fluid teal basic button">Save Draft</button>
                        </div>
                    </div>

                    
                </form>
        </React.Fragment>
        )
    }

    render(){
        return(
        <React.Fragment>
            <div className="ui centered grid">
                <div className="ui compact segment"> 
                    <button className="ui teal float-left basic button" onClick={() => this.props.setProjectView('all')}><i class="arrow left icon"></i> Projects</button>
                    <div className="ui hidden clearing divider"></div>
                    {this.renderView()}
                </div>
            </div>
        </React.Fragment>
        )
    }
}

const formWrapped = reduxForm({
        form: 'addProject'
    })(NewProject);

const mapStateToProps = state => {
    return{
        projects: state.project.allProjects,
        project: state.project.singleProject,
        views: state.project.views.view,
        status: state.project.views.status
    }
}

export default connect(mapStateToProps, {setProjectView, createProject, clearProjectStatus})(formWrapped);