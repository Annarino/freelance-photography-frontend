import React from 'react';
import {connect} from 'react-redux';

import AllBids from '../bidViews/AllBids';

import {clearProjectStatus, setProjectView, editProject, fetchProject} from '../../../actions/projectActions';
import {fetchProjectBids, fetchBid} from '../../../actions/bidActions';
import {ALL} from '../../../actions/types';

class SingleProject extends React.Component{
    constructor(props){
        super(props);
        this.props.fetchProjectBids(this.props.project._id)
    }
  

    componentWillUnmount(){
        
    }

    updateProjectStatus(status){
        this.props.editProject({_id:this.props.project._id, status})
        this.props.fetchProject(this.props.project._id)
    }


    renderEditStatusButton(){
        if(this.props.project.status === 'open'){
            return <div className="ui red right floated button" onClick={() => this.updateProjectStatus('closed')}>Close Bidding</div>
        }
        return <div className="ui green right floated button" onClick={()=>this.updateProjectStatus('open')}>Open Bidding</div>
    }

    renderPhotographer(){
        if(this.props.project.status === 'closed'){
            return(
                <React.Fragment>
                    <div className="ui divider"></div>
                    <div className="ui container">
                        <h2>Accepted Photographer(s):</h2>
                        <div className="ui card">
                            <div className="image">
                                <img src="https://semantic-ui.com/images/avatar/large/daniel.jpg" />
                            </div>
                            <div className="content">
                                <a className="header">Daniel Louise</a>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
                
            )
        }
    }

    render(){
        return(
            <div className="ui very padded segment">
                <div className="ui row">
                    <div className="ui basic teal button" onClick={() => this.props.setProjectView(ALL)}><i className="arrow left icon"></i>Projects</div>
                    <div className="ui primary right floated button">Edit Project</div>
                    {this.renderEditStatusButton()}
                </div>
                <h1>Project Details</h1>
                <div className="ml-5">
                    <h3>Posted on: {new Date(this.props.project.dateCreated).toLocaleDateString()}</h3>
                    <h3>{new Date(this.props.project.startDate).toLocaleDateString()} - {new Date(this.props.project.endDate).toLocaleDateString()}</h3>
                    <h3><i className="map marker alternate icon"></i> {this.props.project.location}</h3>
                    <h3>£{this.props.project.budgetMin} - £{this.props.project.bidgetMax}</h3>
                    <h3>Description:</h3>
                    <p>{this.props.project.description}</p>
                </div>
                {this.renderPhotographer()}
                <h1>Bids: </h1>
                {this.props.bids.length>0?<AllBids /> : <h2>No bids yet!</h2>}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        projects: Object.values(state.project.allProjects),
        project: state.project.singleProject,
        status: state.project.views.status,
        view: state.project.views.view,
        bids: Object.values(state.bid.bids)
    }
}

export default connect(mapStateToProps,{fetchProjectBids, fetchBid, setProjectView, editProject, fetchProject})(SingleProject)