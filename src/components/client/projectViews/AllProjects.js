import React from 'react';
import {connect} from 'react-redux';

import {fetchUsersProjects, setProjectView, setProjectStatus, setSingleProject} from '../../../actions/projectActions';    
import{ALL, SINGLE, ADD, EDIT, CLEAR, SUCCESS, ERROR, LOADING} from '../../../actions/types';

class AllProjects extends React.Component{
    componentWillMount(){
        this.props.fetchUsersProjects();
    }

    componentWillUnmount(){
        this.props.setProjectStatus(CLEAR);
    }

    renderAddProject = () => {
        this.props.setProjectView(ADD);
    }

    renderSingleProject = project => {
        this.props.setSingleProject(project);
        this.props.setProjectView(SINGLE)
    }

    renderView = () => {
        if(this.props.projects.length === 0){
            return(
                <React.Fragment>
                    <h1>No Projects!</h1>
                    <button className="ui button" onClick={this.renderAddProject}>Add Project</button>
                </React.Fragment>
            )
        }
        return this.props.projects.map((project, index) => {
            return(
                <React.Fragment key={index} >
                    <div className="ui raised fluid link card" onClick={() => this.renderSingleProject(project)}>
                        <div className="content">
                            <div className="header" >{project.title}</div>
                        </div>
                        <div className="content">
                            <h4>Posted: {project.datePosted}</h4>
                            <h4>Dates: {new Date(project.startDate).toLocaleDateString()} - {new Date(project.endDate).toLocaleDateString()}</h4>
                            <h4>Budget: £{project.budgetMin} - {project.budgetMax}</h4>
                            <h4>Status: {project.status}</h4>
                            <h4>View Proposals</h4>
                        </div>
                    </div>
                </React.Fragment>
            )
            })
    }

    render(){
        return(
        <div className={this.props.status==='loading'? "ui active centered inline loader grid":"ui container"}>
            <div className="row mb-4">
                <button className="ui green basic button" onClick={this.renderAddProject}><i className="plus icon"></i>Add Project</button>
            </div>
            <div className="row">
                <div className="ui three stackable cards">
                    {this.renderView()}
                </div>
            </div>
            
        </div>)
    }
}

const mapStateToProps = state => {
    return{
        projects: Object.values(state.project.allProjects),
        project: state.project.singleProject,
        views: state.project.views.view,
        status: state.project.views.status
    }
}

export default connect(mapStateToProps,{setProjectView, setSingleProject, fetchUsersProjects, setProjectStatus})(AllProjects);