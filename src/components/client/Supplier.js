import React from 'react';
import {connect} from 'react-redux';

import{fetchSuppliers} from '../../actions/supplierActions';
import{ALL, SINGLE, CLEAR, SUCCESS, ERROR} from '../../actions/types';

class Supplier extends React.Component{
    componentWillMount(){
        this.props.fetchSuppliers();
    }
    render(){
        return(
            <h1>Supplier</h1>
        )
    }
}

const mapStateToProps = state => {
    return{

    }
}

export default connect(mapStateToProps,{fetchSuppliers})(Supplier);