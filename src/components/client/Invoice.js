import React from 'react';
import { connect } from 'react-redux';
import{fetchInvoices, setSingleInvoiceView} from '../../actions';
import SingleInvoice from './SingleInvoice';

class AllInvoices extends React.Component{
    componentDidMount(){
        if(this.props.invoices === undefined || this.props.invoices.allInvoices === undefined){
            this.props.fetchInvoices();
        }
    }

    refreshRecords(){
        this.props.fetchInvoices();
    }
    
    rowClick = invoice => {
        this.props.setSingleInvoiceView(invoice);
    }

    pastDue(issueDate, paymentWindow){
        let pastDue = new Date(issueDate);
        pastDue.setDate(pastDue.getDate() + paymentWindow)
        pastDue = pastDue.toDateString();
        return pastDue;
    }

    renderRecords = () => {
        return this.props.allInvoices.map((invoice, index) => {
            return(
                <tr key={index} onClick={() => this.rowClick(invoice)}>
                    <td data-label="Invoice Number">{invoice.invoiceNumber}</td>
                    <td data-label="Issue Date">{new Date(invoice.issueDate).toDateString()}</td>
                    <td data-label="Past Due Date">{this.pastDue(invoice.issueDate, invoice.paymentWindow)}</td>
                    <td data-label="Client">{invoice.clientInformation.companyName ? invoice.clientInformation.companyName: invoice.clientInformation.firstName+" "+invoice.clientInformation.surame}</td>
                    <td data-label="Project">{invoice.projectId ? invoice.projectId : "N/A"}</td>
                    <td data-label="Status">{invoice.status}</td>
                    <td data-label="Sent to Client">No</td>
                </tr>
            )
        })
    }

    renderView(){
        if(this.props.invoiceViews.singleView === true){
            return <SingleInvoice />
        }
        return(
            <div className="ui container">
                <button className="mini ui button" onClick={() => this.refreshRecords()}>Refresh</button>
                <table className="ui selectable  celled table">
                    <thead>
                        <tr>
                            <th>Invoice Number</th>
                            <th>Issue Date</th>
                            <th>Past Due Date</th>
                            <th>Client</th>
                            <th>Project</th>
                            <th>Status</th>
                            <th>Sent to Client?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRecords()}
                    </tbody>
                </table>
            </div>
        )
    }

    render(){
        return(this.renderView())
    }
}

const mapStateToProps = state => {
    return{
        allInvoices: Object.values(state.invoice.allInvoices),
        invoiceViews: state.invoice.views
    }
}

export default connect(mapStateToProps, {fetchInvoices, setSingleInvoiceView})(AllInvoices);