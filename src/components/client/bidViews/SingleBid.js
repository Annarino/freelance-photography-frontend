import React from 'react';
import {connect} from 'react-redux';

class SingleBid extends React.Component{
    render(){
        return(
            <h1>Single</h1>
        )
    }
}

const mapStateToProps = state => {
    return{
        bids: Object.values(state.bid.bids),
        bid: state.bid.bid,
        status: state.bid.views.status,
        view: state.bid.views.view
    }
}

export default connect(mapStateToProps)(SingleBid);