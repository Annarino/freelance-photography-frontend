import React from 'react';
import {connect} from 'react-redux';
import server from '../../../apis/server'

import ActionModal from '../../global/modals/ActionModal';

import{fetchProjectBids, setBidView, editBid, setSingleBid} from '../../../actions/bidActions';
import {setSingleProject, fetchProject} from '../../../actions/projectActions';
import{setSuppliers} from '../../../actions/supplierActions'
import {MODAL, CLEAR} from '../../../actions/types';

class AllBids extends React.Component{
    constructor(props){
        super(props);
        this.props.fetchProjectBids(this.props.project._id);
    }

    findAcceptedSuppliers = () => {
        let suppliers= [];
        this.props.bids.forEach(bid =>{
            if(bid.status === 'accepted' && !this.props.suppliers.some(supplier => supplier._id !== bid.supplierId._id)){
                console.log('match')
                this.props.setSuppliers(bid.supplierId)
            }
        })
    }

    setStatus(status){
        this.props.editBid({_id:this.props.bid._id, status})
        this.props.fetchProjectBids(this.props.project._id);
    }

    renderModalActions(){
        return(
            <React.Fragment>
                <div className='ui green button' onClick={() => this.setStatus('accepted')}>Accept</div>
                <div className='ui negative button' onClick={() => this.setStatus('rejected')}>Reject</div>
                <div className='ui deny button' onClick={() => this.setStatus('pending')}>Pending</div>
            </React.Fragment>
        )
    }

    renderModal(){
        if(this.props.view === MODAL)
        {return(
            <ActionModal 
            hideModal={() => this.props.setBidView(CLEAR)}
            header={this.props.bid.supplierId}
            content="Would you like to accept or reject this photographers bid?"
            actions={this.renderModalActions()}
            />
        )}
        return;
    }

    updateStatus(bid){
        this.props.setSingleBid(bid);
        this.props.setBidView(MODAL);
    }

    getProposal(slug){
        server.defaults.headers.common['Authorization'] = 'Bearer '+localStorage.getItem('authToken');
        server
            .get(slug, {'responseType':'blob'})
            .then(response => {
                const file = new Blob([response.data], {type:'application/pdf'});
                const fileURL = URL.createObjectURL(file);
                window.open(fileURL)
            })
    }

    renderRecords = () => {
        return this.props.bids.map((bid, index) =>{

            return(
                <tr key={index}>
                    <td className="selectable" data-label="Supplier">{bid.supplierProfileId.firstName} {bid.supplierProfileId.surname}</td>
                    <td className="selectable" data-label="Proposal"><a onClick={() => this.getProposal(bid.proposalURL)}>View Proposal</a></td>
                    <td className="selectable" data-label="Bid Status" onClick={() => this.updateStatus(bid)}>{bid.status} (Click to change)</td>
                </tr>
            )
        })
    }

    renderView(){
        if(this.props.bids.length === 0){
            return(
                <h1>No Bids</h1>
            )
        }
        return(
            <table className="ui celled table">
                {this.renderModal()}
                <thead>
                    <tr>
                        <th>Photographer</th>
                        <th>Proposal</th>
                        <th>Bid Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRecords()}
                </tbody>
            </table>
        )
    }
    render(){
        return (this.renderView())
    }
}

const mapStateToProps = state => {
    return{
        bids: Object.values(state.bid.bids),
        bid: state.bid.bid,
        status: state.bid.views.status,
        view: state.bid.views.view,
        project: state.project.singleProject,
        suppliers: Object.values(state.supplier.suppliers)
    }
}

export default connect(mapStateToProps, {fetchProjectBids, setSingleProject, fetchProject, setBidView, editBid, setSingleBid, setSuppliers})(AllBids);