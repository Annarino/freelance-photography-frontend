import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import AuthButton from '../global/AuthButton';
import RegisterButton from '../global/RegisterButton';

import {clearSingleInvoiceView} from '../../actions';
import {clearProjectView} from '../../actions/projectActions';


class Header extends React.Component{
    render(){
        return (
            <React.Fragment>
                <div style={{padding: '10px 20px'}} className="ui secondary menu">
                <NavLink activeClassName="active" to="/dashboard" className="item">Dashboard</NavLink>
                <NavLink activeClassName="active" to="/invoices" onClick={() => this.props.clearSingleInvoiceView()} className="item">Invoices</NavLink>
                <NavLink activeClassName="active" to="/projects" onClick={() => this.props.clearProjectView()} className="item">Projects</NavLink>
                <NavLink activeClassName="active" to="/photographers" className="item">Browse Photographers</NavLink>
                <div className="right menu">
                    <NavLink activeClassName="active" to="/my-profile" className="item">
                    <i className="user icon big" ></i>
                    </NavLink>
                    <AuthButton />
                </div>
            </div>
            </React.Fragment>
        );
    }
}

export default connect(null, {clearSingleInvoiceView, clearProjectView})(Header);