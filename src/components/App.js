import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import{ initialise } from '../actions';

import Routes from './global/Routes';

class App extends React.Component{
    componentDidMount(){
        this.props.initialise();
    }
    render(){
        return(<div><Routes /></div>)
    }
}

export default connect(null, {initialise})(App);
